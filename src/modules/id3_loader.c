#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "empdd.h"
#include "empc.h"
#include <Ecore.h>
#include <Efreet.h>
#include <Elementary.h>
#include <sys/stat.h>
#include <id3tag.h>

#define TAG_IMG "APIC"
#define TAG_ULYR "USLT"

typedef struct Iinfo
{
   char *uri;
   Empc_Fetch_Request *req;
} Iinfo;

static const char *suffixes[] =
{
   ".jpg",
   ".jpeg",
   ".png",
   ".gif",
   NULL
};

static Eina_Bool
suffix_check(const char *uri)
{
   const char **suffix;

   for (suffix = suffixes; *suffix; suffix++)
     if (eina_str_has_extension(uri, *suffix)) return EINA_TRUE;
   return EINA_FALSE;
}

static void *
id3_search(const char *uri, Eina_Bool album, Eina_Bool txt)
{
   struct id3_file *f;
   struct id3_tag *tag = NULL;
   unsigned int fid;
   struct id3_frame *fr;
   void *bb = NULL;

   f = id3_file_open(uri, ID3_FILE_MODE_READONLY);
   if (f)
     tag = id3_file_tag(f);
   if (!tag) return NULL;
   for (fid = 0; fid < tag->nframes; fid++)
     {
        union id3_field *fi;
        size_t len;
        const unsigned char *img;

        if (txt)
          {
             const Eina_Unicode *u;
             int size;
             char *str;

             fr = id3_tag_findframe(tag, TAG_ULYR, fid);
             if (!fr) continue;

             fi = id3_frame_field(fr, 3);
             u = (const Eina_Unicode*)id3_field_getfullstring(fi);
             str = eina_unicode_unicode_to_utf8(u, &size);
             bb = eina_strbuf_manage_new_length(str, size);
             break;
          }
        else
          {
             fr = id3_tag_findframe(tag, TAG_IMG, fid);
             if (!fr) continue;
             fi = id3_frame_field(fr, 2);
             if ((album && (id3_field_getint(fi) == 3)) ||
                 ((!album) && (id3_field_getint(fi) == 8)))
               {
                  bb = eina_binbuf_new();
                  img = id3_field_getbinarydata(id3_frame_field(fr, 4), &len);
                  eina_binbuf_append_length(bb, img, len);
                  break;
               }
          }
        fid++;
     }
   id3_file_close(f);
   return bb;
}

static void
id3notify(Iinfo *ii, Ecore_Thread *eth EINA_UNUSED, void *bb)
{
   Eina_List *l, *ll;
   Evas_Object *o;
   Eina_File *f;

   if (ii->req->type == EMPC_METADATA_TYPE_IMAGE)
     {
        Eina_Bool fail = EINA_FALSE;

        f = eina_file_virtualize(ii->req->uri,
                                 eina_binbuf_string_steal(bb),
                                 eina_binbuf_length_get(bb),
                                 EINA_FALSE);
        l = empc_metadata_images_add(ii->req);
        EINA_LIST_FOREACH(l, ll, o)
          {
             evas_object_data_set(o, "__empc_nosave", (void*)1);
             if (!elm_image_mmap_set(o, f, NULL))
               {
                  fail = EINA_TRUE;
                  break;
               }
          }
        if (fail)
          E_FREE_LIST(l, evas_object_del);
        if (l)
          empc_metadata_fetch_done(ii->req, l);
        eina_file_close(f);
        eina_binbuf_free(bb);
        return;
     }
   l = empc_metadata_entries_add(ii->req);
   EINA_LIST_FOREACH(l, ll, o)
     {
        evas_object_data_set(o, "__empc_nosave", (void*)1);
        evas_object_data_set(o, "__empc_override", (void*)1);
        elm_entry_entry_set(o, eina_strbuf_string_get(bb));
     }
   if (l)
     empc_metadata_fetch_done(ii->req, l);
   eina_strbuf_free(bb);
}

static void
id3end(Iinfo *ii, Ecore_Thread *eth EINA_UNUSED)
{
   ii->req->running = 0;
   empc_metadata_fetch_done(ii->req, NULL);
   free(ii->uri);
   free(ii);
}

static Eina_List *
id3scan(Eina_Iterator *it, Eina_List *files, Eina_Bool recursive)
{
   Eina_File_Direct_Info *info;

   EINA_ITERATOR_FOREACH(it, info)
     {
        if (info->type != EINA_FILE_REG)
          {
             Eina_Iterator *it2;

             if ((!recursive) || (info->type != EINA_FILE_DIR))
               continue;
             it2 = eina_file_stat_ls(info->path);
             files = id3scan(it2, files, recursive);
          }
        if (!suffix_check(info->path)) continue;
        files = eina_list_append(files, strdup(info->path));
     }
   return files;
}

static void
id3run(Iinfo *ii, Ecore_Thread *eth)
{
   
   Eina_Iterator *it;
   struct stat st;
   Eina_Bool isdir;
   char *uri;
   Eina_List *files = NULL;
   void *bb = NULL;

   if (stat(ii->uri, &st)) return;
   isdir = S_ISDIR(st.st_mode);
   if (!isdir)
     {
        bb = id3_search(ii->req->uri, !!ii->req->album, ii->req->type == EMPC_METADATA_TYPE_TEXT);
        if (bb)
          ecore_thread_feedback(eth, bb);
        return;
     }

   it = eina_file_stat_ls(ii->uri);
   files = id3scan(it, files, !!ii->req->album);

   eina_iterator_free(it);
   EINA_LIST_FREE(files, uri)
     {
        if (!bb)
          bb = id3_search(uri, !!ii->req->album, ii->req->type == EMPC_METADATA_TYPE_TEXT);
        free(uri);
     }
   if (bb)
     ecore_thread_feedback(eth, bb);
}

EAPI Eina_Bool
empc_module_metadata_fetch(Empc_Fetch_Request *req)
{
   char buf[PATH_MAX];
   Iinfo *ii;

   if ((!req->uri) || (!empd_music_directory)) return EINA_FALSE;
   req->running = 1;

   snprintf(buf, sizeof(buf), "%s/%s", empd_music_directory, req->uri);
   ii = malloc(sizeof(Iinfo));
   ii->uri = strdup(buf);
   ii->req = req;
   ecore_thread_feedback_run((Ecore_Thread_Cb)id3run, (Ecore_Thread_Notify_Cb)id3notify, (Ecore_Thread_Cb)id3end, (Ecore_Thread_Cb)id3end, ii, 0);
   return EINA_TRUE;
}

EAPI Empc_Module_Type
empc_module_type(void)
{
   return EMPC_MODULE_TYPE_METADATA_FETCH;
}

EAPI int
empc_module_priority(void)
{
   return 9;
}

static Eina_Bool
id3_init(void)
{
   return EINA_TRUE;
}

static void
id3_shutdown(void)
{
}

EINA_MODULE_INIT(id3_init);
EINA_MODULE_SHUTDOWN(id3_shutdown);
