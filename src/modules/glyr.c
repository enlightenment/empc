#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "empdd.h"
#include "empc.h"
#include <Ecore.h>
#include <Efreet.h>
#include <Elementary.h>

static Eina_List *exes = NULL;
static Eina_List *handlers = NULL;

static char replace_chars[] =
{
   ' ',
   '\t',
   '\n',
   '\\',
   '\'',
   '\"',
   ';',
   '!',
   '#',
   '$',
   '%',
   '&',
   '*',
   '(',
   ')',
   '[',
   ']',
   '{',
   '}',
   '|',
   '<',
   '>',
   '?',
   0
};

static char *
_escape(const char *str)
{
   Eina_Strbuf *buf;
   const char *p;
   char *ret;

   if (!str) return NULL;
   buf = eina_strbuf_new();
   for (p = str; p[0]; p++)
     {
        const char *c;

        for (c = replace_chars; c[0]; c++)
          {
             if (c[0] != p[0]) continue;
             eina_strbuf_append_char(buf, '\\');
             break;
          }
        eina_strbuf_append_char(buf, p[0]);
     }
   ret = eina_strbuf_string_steal(buf);
   eina_strbuf_free(buf);
   return ret;
}

static Eina_Bool
exe_data(void *d EINA_UNUSED, int t EINA_UNUSED, Ecore_Exe_Event_Data *ev)
{
   Empc_Fetch_Request *req;
   const char *tag;
   Ecore_Exe_Event_Data_Line *el;

   tag = ecore_exe_tag_get(ev->exe);
   if ((!eina_list_data_find_list(exes, ev->exe)) || (!tag) || strcmp(tag, "glyr"))
     return ECORE_CALLBACK_RENEW;
   req = ecore_exe_data_get(ev->exe);
   for (el = ev->lines; el && el->line ; el++)
     {
        if (el->line[0] != 'h') continue;
        if (strncmp(el->line, "http", 4)) continue;
        if (req->force)
          empc_metadata_image_download(req, el->line);
        else
          empc_metadata_image_download_queue(req, el->line);
     }
   return ECORE_CALLBACK_DONE;
}

static Eina_Bool
exe_del(void *d EINA_UNUSED, int t EINA_UNUSED, Ecore_Exe_Event_Del *ev)
{
   Eina_List *l;
   Empc_Fetch_Request *req;
   const char *tag;

   if (!ev->exe) return ECORE_CALLBACK_RENEW;
   tag = ecore_exe_tag_get(ev->exe);
   l = eina_list_data_find_list(exes, ev->exe);
   if (!l) return (!tag) || strcmp(tag, "glyr");
   exes = eina_list_remove_list(exes, l);
   if ((!tag) || strcmp(tag, "glyr")) return ECORE_CALLBACK_DONE;
   ecore_exe_tag_set(ev->exe, NULL);
   req = ecore_exe_data_get(ev->exe);
   req->running = 0;
   empc_metadata_fetch_done(req, NULL);
   return ECORE_CALLBACK_DONE;
}

EAPI void
empc_module_metadata_cancel(const Empc_Fetch_Request *req)
{
   Eina_List *l, *ll;
   Ecore_Exe *exe;

   EINA_LIST_FOREACH_SAFE(exes, l, ll, exe)
     {
        if (ecore_exe_data_get(exe) != req) continue;
        exes = eina_list_remove_list(exes, l);
        ecore_exe_tag_set(exe, NULL);
        ecore_exe_terminate(exe);
     }
}

EAPI Eina_Bool
empc_module_metadata_fetch(Empc_Fetch_Request *req)
{
   char buf[4096];
   char *artist, *album;
   Ecore_Exe *exe;
   const char *album_cmd[] =
   {
      "glyrc cover -D -v1 --write /dev/null -n1",
      "glyrc cover -D -v1 --write /dev/null -n10",
   };
   const char *artist_cmd[] =
   {
      "glyrc artistphoto -D -v1 --write /dev/null -n1",
      "glyrc artistphoto -D -v1 --write /dev/null -n10",
   };

   if (req->type != EMPC_METADATA_TYPE_IMAGE) return EINA_FALSE;
   artist = _escape(req->artist);
   album = _escape(req->album);
   if (album)
     snprintf(buf, sizeof(buf), "%s -a %s -b %s", album_cmd[req->force], artist, album);
   else
     snprintf(buf, sizeof(buf), "%s -a %s", artist_cmd[req->force], artist);
   exe = ecore_exe_pipe_run(buf, ECORE_EXE_PIPE_ERROR | ECORE_EXE_PIPE_ERROR_LINE_BUFFERED, req);
   if (exe)
     {
        req->running = 1;
        ecore_exe_tag_set(exe, "glyr");
        exes = eina_list_append(exes, exe);
     }

   return !!exe;
}

EAPI Eina_Bool
empc_module_remote(void)
{
   return EINA_TRUE;
}

EAPI Empc_Module_Type
empc_module_type(void)
{
   return EMPC_MODULE_TYPE_METADATA_FETCH;
}

EAPI int
empc_module_priority(void)
{
   return 50;
}

static Eina_Bool
glyr_init(void)
{
   ecore_init();
   E_LIST_HANDLER_APPEND(handlers, ECORE_EXE_EVENT_ERROR, exe_data, NULL);
   E_LIST_HANDLER_APPEND(handlers, ECORE_EXE_EVENT_DEL, exe_del, NULL);
   return EINA_TRUE;
}

static void
glyr_shutdown(void)
{
   E_FREE_LIST(handlers, ecore_event_handler_del);
   E_FREE_LIST(exes, ecore_exe_free);
   ecore_shutdown();
}

EINA_MODULE_INIT(glyr_init);
EINA_MODULE_SHUTDOWN(glyr_shutdown);
