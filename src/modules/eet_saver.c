#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "empdd.h"
#include "empc.h"
#include "eet_hash.h"
#include <Ecore.h>
#include <Efreet.h>
#include <Elementary.h>
#include <Eet.h>
#include <Eio.h>

static Eet_File *ef_img = NULL;
static Eet_File *ef_lyr = NULL;

static void
tcb()
{
}

static void
idcb(void *data, Eio_File *h EINA_UNUSED)
{
   Empc_Module_Metadata_Save_Post_Cb post_cb = data;

   if (post_cb) post_cb();
}

static void
iecb(void *data, Eio_File *h EINA_UNUSED, Eet_Error err EINA_UNUSED)
{
   Empc_Module_Metadata_Save_Post_Cb post_cb = data;

   if (post_cb) post_cb();
}

EAPI void
empc_module_metadata_save_image(Evas_Object *obj, const char *uri, const char *artist, const char *album, Empc_Module_Metadata_Save_Post_Cb post_cb)
{
   Evas_Object *o;
   void *img;
   int w, h;
   char buf[4096];
   char *a = NULL, *b = NULL;

   if (artist)
     {
        a = strdupa(artist);
        eina_str_tolower(&a);
     }
   if (album)
     {
        b = strdupa(album);
        eina_str_tolower(&b);
     }
   snprintf(buf, sizeof(buf), "%s:::%s", a ?: "", b ?: "");
   if (obj)
     {
        if (evas_object_data_get(obj, "__empc_nosave"))
          {
             Eina_Stringshare *f;

             /* filesystem loader image; save location to hash but not image */
             elm_image_file_get(obj, &f, NULL);
             eet_delete(ef_img, buf);
             eet_hash_set(ef_img, buf, f);
          }
        else
          {
             const Eina_File *eif;

             o = elm_image_object_get(obj);
             img = evas_object_image_data_get(o, EINA_FALSE);
             if (img)
               {
                  evas_object_image_size_get(o, &w, &h);
                  eet_data_image_write(ef_img, buf, img, w, h, 0, 1, 100, 0);
               }
             evas_object_image_mmap_get(o, &eif, NULL);
             eet_hash_set(ef_img, buf, eina_file_filename_get(eif));
          }
     }
   else if (uri)
     eet_write(ef_img, buf, uri, strlen(uri), 1);
   else
     {
        eet_delete(ef_img, buf);
        eet_hash_set(ef_img, buf, NULL);
     }
   eio_eet_sync(ef_img, idcb, iecb, post_cb);
}

EAPI void
empc_module_metadata_save_text(const char *text, const char *artist, const char *song)
{
   char buf[4096];
   char *a = NULL, *b = NULL;

   if (artist)
     {
        a = strdupa(artist);
        eina_str_tolower(&a);
     }
   if (song)
     {
        b = strdupa(song);
        eina_str_tolower(&b);
     }
   snprintf(buf, sizeof(buf), "%s:::%s", a ?: "", b ?: "");
   eet_write(ef_lyr, buf, text, strlen(text) + 1, 1);
   eio_eet_sync(ef_lyr, tcb, tcb, NULL);
}

EAPI Empc_Module_Type
empc_module_type(void)
{
   return EMPC_MODULE_TYPE_METADATA_SAVER;
}

EAPI int
empc_module_priority(void)
{
   return 50;
}

static Eina_Bool
eet_saver_init(void)
{
   char buf[PATH_MAX];

   eet_init();
   eet_hash_init();
   snprintf(buf, sizeof(buf), "%s/empc/metadata/images.eet", efreet_cache_home_get());
   ef_img = eet_open(buf, EET_FILE_MODE_READ_WRITE);
   snprintf(buf, sizeof(buf), "%s/empc/metadata/lyrics.eet", efreet_cache_home_get());
   ef_lyr = eet_open(buf, EET_FILE_MODE_READ_WRITE);

   return !!ef_img && !!ef_lyr;
}

static void
eet_saver_shutdown(void)
{
   eet_close(ef_img);
   eet_close(ef_lyr);
   eet_hash_shutdown();
   eet_shutdown();
}

EINA_MODULE_INIT(eet_saver_init);
EINA_MODULE_SHUTDOWN(eet_saver_shutdown);
