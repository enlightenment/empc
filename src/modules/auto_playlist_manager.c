#ifdef HAVE_CONFIG_H
# include "config.h"
#endif


#include "empdd.h"
#include "empc.h"
#include <Ecore.h>
#include <Eio.h>
#include <Elementary.h>
#include "eldbus_empd_empdd.h"

static Eina_Stringshare *log_file;
static Eio_Monitor *log_monitor;
static Eina_List *handlers;
static size_t log_size;
static Eina_Bool log_dirty;
static Ecore_Timer *update_timer;

static Eina_List *adds;
static unsigned int wait_count;
static unsigned int last_queue_length;

static void auto_playlist_manager_shutdown(void);

static Eina_Bool
monitor_init(void)
{
   Eina_File *f;

   f = eina_file_open(log_file, 0);
   if (!f) return EINA_FALSE;
   log_size = eina_file_size_get(f);
   eina_file_close(f);
   E_FREE_FUNC(log_monitor, eio_monitor_del);
   log_monitor = eio_monitor_stringshared_add(log_file);
   return EINA_TRUE;
}

static Eina_Bool
log_removed(void *d EINA_UNUSED, int t EINA_UNUSED, Eio_Monitor_Event *ev)
{
   if (ev->filename != log_file) return ECORE_CALLBACK_RENEW;
   if (!monitor_init())
     auto_playlist_manager_shutdown();
   return ECORE_CALLBACK_RENEW;
}

static Eina_Bool
log_modified(void *d EINA_UNUSED, int t EINA_UNUSED, Eio_Monitor_Event *ev)
{
   if (!master) return ECORE_CALLBACK_RENEW;
   if (ev->filename != log_file) return ECORE_CALLBACK_RENEW;
   log_dirty = EINA_TRUE;
   return ECORE_CALLBACK_RENEW;
}

#if 0
static inline const char *
get_eol(const char *p, const char *txt, size_t size)
{
   const char *e = p;

   while ((e[0] != '\n') && (e[0] != '\r') && ((unsigned int)(e - txt) < size))
     e++;
   return e;
}
#endif
/* assuming sensible directory structure of $ARTIST/$ALBUM/$TRACK-$TITLE.$EXT
 * then this should, at the least, add albums in sequential order even if
 * they don't necessarily appear in the playlist in the order that they were
 * added to the database
 */
static int
sort_cb(const char *a, const char *b)
{
   return strcmp(a, b);
}

static inline Eina_Bool
is_same_path(const char *a, const char *ae, const char *b, const char *be)
{
   const char *suba = NULL, *subb = NULL;
   if (!ae)
     {
        ae = strrchr(a, '/');
        if (ae) ae++;
     }
   if (!be)
     {
        be = strrchr(b, '/');
        if (be) be++;
     }
   /* if full match on all directories, match */
   if ((ae - a == be - b) && (!memcmp(a, b, ae - a))) return EINA_TRUE;
   if (ae)
     suba = memrchr(a, '/', ae - a - 1);
   if (be)
     subb = memrchr(b, '/', be - b - 1);
   /* try to match subsections of directories:
    * Artist/Album/CD 1/song
    * Artist/Album/CD 2/song2
    * this is valid
    */
   if (suba && subb)
     {
        if (suba - a != subb - b) return EINA_FALSE;
        return !memcmp(a, b, suba - a);
     }
   return EINA_FALSE;
}

static void
post_add(Eina_Bool insert, const char *filename)
{
   char summary[1024], body[PATH_MAX + 128];

   snprintf(summary, sizeof(summary), "EMPC: Auto Playlist Manager");
   snprintf(body, sizeof(body), "%s '%s'", insert ? "Insert:" : "Append:", filename);
   elm_sys_notify_send(0, "empc", summary, body, ELM_SYS_NOTIFY_URGENCY_NORMAL, 5000, NULL, NULL);
   wait_count++;
}

static void
info_cb(Eldbus_Proxy *proxy EINA_UNUSED, void *data, Eldbus_Pending *pending EINA_UNUSED, Eldbus_Error_Info *error EINA_UNUSED, Eina_Value *args)
{
   Eina_Value array;
   unsigned int i;

   if (!args) return; //canceled
   if (!eina_value_struct_value_get(args, "arg0", &array)) return;
   for (i = 0; i < eina_value_array_count(&array); i++)
     {
        Eina_Value struc, val, realval;
        int type;
        Empd_Empdd_File *file = NULL;

        eina_value_array_value_get(&array, i, &struc);
        eina_value_struct_get(&struc, "arg0", &type);
        eina_value_struct_value_get(&struc, "arg1", &val);
        eina_value_struct_value_get(&val, "arg0", &realval);
        switch (type)
          {
           case MPD_ENTITY_TYPE_DIRECTORY:
           case MPD_ENTITY_TYPE_PLAYLIST:
             break;
           case MPD_ENTITY_TYPE_SONG:
             if (azy_value_to_Empd_Empdd_File(&realval, &file))
               {
                  const Eina_List *l, *ll;
                  Elm_Object_Item *it;
                  Empd_Empdd_Song *so;

                  l = empc_modapi_queue_list_header_items_find(file->artist, file->album);
                  if (!l)
                    {
                       /* no match: append */
                       empd_empdd_add_list_call(empd_proxy, data);
                       post_add(0, data);
                       break;
                    }
                  if (!file->track)
                    {
                       so = eina_list_last_data_get(l);
                       empd_empdd_add_id_list_call(empd_proxy, data, so->song_pos + 1);
                       post_add(1, data);
                       break;
                    }
                  /* matches: insert at end point first to avoid breaking song positions */
                  EINA_LIST_REVERSE_FOREACH(l, ll, it)
                    {
                       const Eina_List *sl, *sll;
                       Elm_Object_Item *sit;

                       sl = elm_genlist_item_subitems_get(it);
                       if (eina_list_count(sl) < 2) continue; //ignore single songs
                       post_add(1, data);
                       EINA_LIST_FOREACH(sl, sll, sit)
                         {
                            so = elm_object_item_data_get(sit);
                            if (so->track > file->track)
                              {
                                 empd_empdd_add_id_list_call(empd_proxy, data, so->song_pos);
                                 break;
                              }
                            if (!sll->next)
                              empd_empdd_add_id_list_call(empd_proxy, data, so->song_pos + 1);
                         }
                    }
               }
             break;
          }
        eina_value_flush(&struc);
        eina_value_flush(&val);
        eina_value_flush(&realval);
     }
   eina_value_flush(&array);
   free(data);
}

static Eina_Bool
run_adds_for_header(Elm_Object_Item *item, Eina_List *todo)
{
   const char *file;
   const Eina_List *sub, *lll;
   Eina_List *l;
   Elm_Object_Item *subi;
   int ret = 0, last_pos = 0;
   Eina_Bool updated = EINA_FALSE;

   sub = elm_genlist_item_subitems_get(item);
   if (eina_list_count(sub) < 2) //just one song, ignore
     return EINA_FALSE;

   EINA_LIST_REVERSE_FOREACH(todo, l, file)
     {
        Eina_Bool update_success = EINA_FALSE;
        /* insert items starting at the end of the subitems to ensure placement */
        EINA_LIST_REVERSE_FOREACH(sub, lll, subi)
          {
             Empd_Empdd_Song *ss = elm_object_item_data_get(subi);

             last_pos = ss->song_pos;
             /* find item that goes before this one and place this after */
             ret = strcmp(ss->uri, file);
             if (!ret) break; //already exists here...what the actual fuck
             if (ret > 0) continue;
             post_add(1, file);
             empd_empdd_add_id_list_call(empd_proxy, file, ss->song_pos + 1);
             update_success = EINA_TRUE;
          }
        /* song goes before any existing songs */
        if (ret && (!update_success))
          {
             empd_empdd_add_id_list_call(empd_proxy, file, last_pos);
             updated = EINA_TRUE;
          }
        updated |= update_success;
     }

   return !!updated;
}

static void
run_adds(void)
{
   char *file;
   Eina_List *todo = NULL;

   last_queue_length = empd_queue_length;
   while (adds)
     {
        const char *a, *b = NULL;
        Eina_Iterator *it;
        Eina_List *i, *ii;
        Elm_Object_Item *item;
        Eina_Bool done = EINA_FALSE;

        file = eina_list_data_get(adds);
        a = strrchr(file, '/');
        if (a)
          b = strchr(file, '/');
        /* messy or poorly-structured music directory; use database info
         *
         * this sucks due to constant round tripping, and imo it's probably
         * worth popping an error dialog over. but empc should Just Work (tm),
         * so I'll just soldier on here like a good little app.
         */
        if (a == b)
          {
             empd_empdd_list_info_call(empd_proxy, info_cb, file, file);
             adds = eina_list_remove_list(adds, adds);
             return;
          }
        adds = eina_list_remove_list(adds, adds);


        /* create list of all items in directory to update at once */
        todo = eina_list_append(todo, file);
        EINA_LIST_FOREACH_SAFE(adds, i, ii, b)
          {
             if (!is_same_path(file, a, b, NULL)) break;
             eina_list_move_list(&todo, &adds, i);
          }

        /* check for existing pieces of album in list */
        a++;
        it = eina_hash_iterator_data_new(empd_current_queue_headers);
        EINA_ITERATOR_FOREACH(it, i)
          {
             Empd_Empdd_Song *so;

             item = eina_list_data_get(i);
             so = elm_object_item_data_get(item);
             if (!is_same_path(file, a, so->uri, NULL)) continue;

             /* run updates for related headers */
             EINA_LIST_REVERSE_FOREACH(i, ii, item)
               done |= run_adds_for_header(item, todo);
             break;
          }
        if (done)
          E_FREE_LIST(todo, free);
        else
          {
             EINA_LIST_FREE(todo, file)
               {
                  /* sequentially append all songs from matching directory structure */
                  empd_empdd_add_list_call(empd_proxy, file);
                  post_add(0, file);
                  free(file);
               }
          }
     }
}

static Eina_Bool
queue_update()
{
   
   if (wait_count) wait_count -= (empd_queue_length - last_queue_length);
   if (adds && (!wait_count))
     run_adds();
   return ECORE_CALLBACK_RENEW;
}

static Eina_Bool
update_timer_cb()
{
   Eina_File *f;
   size_t size;

   update_timer = NULL;
   f = eina_file_open(log_file, 0);
   if (!f) return EINA_FALSE; //FIXME: probably should error or something?
   size = eina_file_size_get(f);
   if (size > log_size)
     {
        /* this should be moderately safe to do synchronously;
         * at most it may drop 1-2 frames, but this is unlikely.
         * the risk is worthwhile since it will avoid conflicts in the event
         * of fast updates...maybe
         */
        FILE *ff;
        char txt[4096];

        ff = fopen(log_file, "rb");
        if (!fseek(ff, log_size, SEEK_SET))
          {
             while (fgets(txt, sizeof(txt), ff))
               {
                  char *p, *e;
                  Eina_List **files = NULL;
                  //v
                  //Oct 09 23:55 : update: added The Algorithm/Octopus4/06-Synthesiz3r.mp3\n
                  //Oct 09 23:55 : update: removing The Algorithm/Octopus4/06-Synthesiz3r.mp3\n
                  p = strstr(txt, ": update: ");
                  if (!p) continue;
                  //             v
                  //Oct 09 23:55 : update: added The Algorithm/Octopus4/06-Synthesiz3r.mp3\n
                  //Oct 09 23:55 : update: removing The Algorithm/Octopus4/06-Synthesiz3r.mp3\n
                  p += sizeof(": update: ") - 1;
                  if (!strncmp(p, "added ", sizeof("added ") - 1))
                    {
                       //                             v
                       //Oct 09 23:55 : update: added The Algorithm/Octopus4/06-Synthesiz3r.mp3\n
                       files = &adds;
                       p += sizeof("added ") - 1;
                    }
                    /*
                  else if (!strncmp(p, "removing ", sizeof("removing ") - 1))
                    {
                       //                                v
                       //Oct 09 23:55 : update: removing The Algorithm/Octopus4/06-Synthesiz3r.mp3\n
                       files = &rems;
                       p += sizeof("removing ") - 1;
                    }
                    */
                  else continue;
                  e = strchr(p, '\n');
                  *files = eina_list_sorted_insert(*files, (Eina_Compare_Cb)sort_cb, strndup(p, e - p));
               }
          }
#if 0 //broken because fuck you mmap
        char *txt;
        txt = eina_file_map_new(f, EINA_FILE_POPULATE, log_size, size - log_size);
        if (txt)
          {
             size_t length = size - log_size, offset = 0;

             while (offset < size)
               {
                  const char *p, *e;

                  //v
                  //Oct 09 23:55 : update: added The Algorithm/Octopus4/06-Synthesiz3r.mp3\n
                  p = memchr(txt, ':', length - offset);
                  if (!p) break;
#define OFFSET offset = (p - txt)
                  OFFSET;
                  if (size - offset < sizeof(": update: added ")) break;
                  if (p[1] != ' ')
                  //         v
                  //Oct 09 23:55 : update: added The Algorithm/Octopus4/06-Synthesiz3r.mp3\n
                    p = memchr(p + 1, ':', length - offset);
                  OFFSET;
                  if (size - offset < sizeof(": update: added ")) break;
                  //             v
                  //Oct 09 23:55 : update: added The Algorithm/Octopus4/06-Synthesiz3r.mp3\n
                  if (!memcmp(++p, " update: added ", sizeof(" update: added ") - 1))
                    {
                       p += sizeof(" update: added ") - 1;
                       OFFSET;
                       //                             v
                       //Oct 09 23:55 : update: added The Algorithm/Octopus4/06-Synthesiz3r.mp3\n
                       e = get_eol(p, txt, size);
                       if ((unsigned int)(e - txt) < size)
                         {
                            char *name;

                            name = malloc(e - p + 1);
                            name[e - p] = 0;
                            memcpy(name, p, e - p);
                            files = eina_list_sorted_insert(files, (Eina_Compare_Cb)sort_cb, name);
                            //                                                                       v
                            //Oct 09 23:55 : update: added The Algorithm/Octopus4/06-Synthesiz3r.mp3\n
                            p = e + 1;
                         }
                       if ((unsigned int)(p - txt) >= size)
                         break;
                       OFFSET;
                    }
                  else
                    {
                       e = get_eol(p, txt, size);
                       if ((unsigned int)(e - txt + 1) >= size)
                         break;
                       p = e + 1;
                       OFFSET;
                    }
               }
          }
        eina_file_map_free(f, txt);
#endif

         if (adds)
           run_adds();
     }
   log_size = size;
   eina_file_close(f);
   log_dirty = EINA_FALSE;
   return EINA_FALSE;
}

static Eina_Bool
db_update()
{
   if (!log_dirty) return ECORE_CALLBACK_RENEW;
   if (update_timer)
     ecore_timer_reset(update_timer);
   else
     update_timer = ecore_timer_add(2.0, update_timer_cb, NULL);
   return ECORE_CALLBACK_RENEW;
}

EAPI Empc_Module_Type
empc_module_type(void)
{
   return EMPC_MODULE_TYPE_MISC;
}

static inline Eina_Bool
skip_spaces(const char **p, const char *end)
{
   while (((*p)[0] == ' ') || ((*p)[0] == '\t') )
     {
        (*p)++;
        if ((*p) == end) return EINA_FALSE;
     }
   return (*p) != end;
}

static void
init_end(Eina_File *f, Ecore_Thread *eth EINA_UNUSED)
{
   eina_file_close(f);
   if (!log_monitor)
     auto_playlist_manager_shutdown();
}

static void
init_notify(void *d EINA_UNUSED, Ecore_Thread *eth EINA_UNUSED, void *msg)
{
   log_file = msg;
   if (log_file)
     monitor_init();
}

static void
init_thread(Eina_File *f, Ecore_Thread *eth)
{
   Eina_Iterator *it;
   Eina_File_Line *l;

   it = eina_file_map_lines(f);
   if (!it) return;
   EINA_ITERATOR_FOREACH(it, l)
     {
        const char *e, *p = l->start;

        if (l->length < sizeof("log_file")) continue;
        if (!skip_spaces(&p, l->end)) continue;
        if (p[0] == '#') continue; //skip comments
        if (l->length - (p - l->start) < sizeof("log_file")) continue;
        if (memcmp(p, "log_file", sizeof("log_file") - 1)) continue;
        p += sizeof("log_file") - 1;
        if (!skip_spaces(&p, l->end)) continue;
        if (p[0] != '"') continue;
        p++;
        if (p == l->end) continue;
        e = memchr(p, '"', l->length - (p - l->start));
        if (!e) continue;
        ecore_thread_feedback(eth, eina_stringshare_add_length(p, (unsigned int)(e - p)));
        break;
     }
   eina_iterator_free(it);
}

static Eina_Bool
auto_playlist_manager_init(void)
{
   Eina_File *f;

   f = eina_file_open("/etc/mpd.conf", 0);
   if (!f) return EINA_FALSE;
   elm_need_sys_notify();
   ecore_thread_feedback_run((Ecore_Thread_Cb)init_thread, (Ecore_Thread_Notify_Cb)init_notify, (Ecore_Thread_Cb)init_end, (Ecore_Thread_Cb)init_end, f, 0);
   eio_init();
   E_LIST_HANDLER_APPEND(handlers, EIO_MONITOR_FILE_MODIFIED, log_modified, NULL);
   E_LIST_HANDLER_APPEND(handlers, EIO_MONITOR_SELF_DELETED, log_removed, NULL);
   E_LIST_HANDLER_APPEND(handlers, EIO_MONITOR_SELF_RENAME, log_removed, NULL);
   E_LIST_HANDLER_APPEND(handlers, EMPD_EMPDD_DATABASE_UPDATE_END_EVENT, db_update, NULL);
   E_LIST_HANDLER_APPEND(handlers, EMPD_EMPDD_QUEUE_LIST_EVENT, queue_update, NULL);
   return EINA_TRUE;
}

static void
auto_playlist_manager_shutdown(void)
{
   E_FREE_FUNC(log_monitor, eio_monitor_del);
   E_FREE_LIST(handlers, ecore_event_handler_del);
   E_FREE_FUNC(update_timer, ecore_timer_del);
   eina_stringshare_replace(&log_file, NULL);
   eio_shutdown();
}

EINA_MODULE_INIT(auto_playlist_manager_init);
EINA_MODULE_SHUTDOWN(auto_playlist_manager_shutdown);
