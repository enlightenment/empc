#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "empdd.h"
#include "empc.h"
#include <Ecore.h>
#include <Efreet.h>
#include <Elementary.h>

#define SEARCH_OFFSET 200000
#define SEARCH_STR "https://www.google.com/search?q=%s&num=10&client=firefox&tbm=isch&tbs=isz:l"

static Eina_List *handlers = NULL;
static Eina_List *reqs = NULL;

typedef struct GImage_Data
{
   Empc_Fetch_Request *req;
   Eina_Strbuf *data;
   size_t size;
} GImage_Data;

static void
gnotify(GImage_Data *gi, Ecore_Thread *eth EINA_UNUSED, char *url)
{
   if (gi->req->force)
     empc_metadata_image_download(gi->req, url);
   else
     empc_metadata_image_download_queue(gi->req, url);
   free(url);
}

static void
gend(GImage_Data *gi, Ecore_Thread *eth EINA_UNUSED)
{
   eina_strbuf_free(gi->data);
   gi->req->running = 0;
   empc_metadata_fetch_done(gi->req, NULL);
   free(gi);
}

static void
gparse(GImage_Data *gi, Ecore_Thread *eth)
{
   const char *s, *e, *p = eina_strbuf_string_get(gi->data);
   unsigned int count = 0;
#define MARKER_ONE "\"ou\":\""

   do {
      char *use, *enc;

      s = strstr(p, MARKER_ONE);
      if (!s) break;
      s += sizeof(MARKER_ONE) - 1;
      e = strchr(s, '"');
      if (!e) break;
      enc = memchr(s, '\\', e - s);
      if (enc)
        {
           Eina_Strbuf *sbuf = eina_strbuf_new();
           p = s;

           /* encoded ascii */
           while (enc)
             {
                int c;
                char buf[5] = {0};

                eina_strbuf_append_length(sbuf, p, enc - p);
                if ((!p[1]) || (!p[2]))
                  {
                     E_FREE_FUNC(sbuf, eina_strbuf_free);
                     break;
                  }
                p = enc += 2;
                strncpy(buf, enc, 4);
                c = strtol(buf, NULL, 16);
                if (c < 0)
                  {
                     E_FREE_FUNC(sbuf, eina_strbuf_free);
                     break;
                  }
                p = enc += 4;
                eina_strbuf_append_char(sbuf, c);
                enc = memchr(enc, '\\', e - p);
             }
           eina_strbuf_append_length(sbuf, p, e - p);
           use = eina_strbuf_string_steal(sbuf);
           eina_strbuf_free(sbuf);
        }
      else
        use = (char*)eina_memdup((void*)s, e - s, 1);

      ecore_thread_feedback(eth, use);
      count++;
      if (count > 10) break;
      p = e;
   } while (1);
}

static Eina_Bool
gdone(void *d EINA_UNUSED, int t EINA_UNUSED, Ecore_Con_Event_Url_Complete *ev)
{
   GImage_Data *gi;

   if ((!reqs) || (!eina_list_data_find(reqs, ev->url_con))) return ECORE_CALLBACK_RENEW;
   gi = ecore_con_url_data_get(ev->url_con);
   reqs = eina_list_remove(reqs, ev->url_con);
   ecore_con_url_free(ev->url_con);

   ecore_thread_feedback_run((Ecore_Thread_Cb)gparse, (Ecore_Thread_Notify_Cb)gnotify, (Ecore_Thread_Cb)gend, (Ecore_Thread_Cb)gend, gi, 0);
   return ECORE_CALLBACK_DONE;
}

static Eina_Bool
gdata(void *d EINA_UNUSED, int t EINA_UNUSED, Ecore_Con_Event_Url_Data *ev)
{
   GImage_Data *gi;

   if ((!reqs) || (!eina_list_data_find(reqs, ev->url_con))) return ECORE_CALLBACK_RENEW;
   //fprintf(stderr, "%*s\n", ev->size, ev->data);
   gi = ecore_con_url_data_get(ev->url_con);
   gi->size += ev->size;
   if (gi->size < SEARCH_OFFSET) return ECORE_CALLBACK_DONE;

   eina_strbuf_append_length(gi->data, (char*)&ev->data[0], ev->size);
   return ECORE_CALLBACK_DONE;
}

EAPI Eina_Bool
empc_module_metadata_fetch(Empc_Fetch_Request *req)
{
   Eina_Strbuf *sb = NULL;
   char buf[4096];
   Ecore_Con_Url *url;
   GImage_Data *gi;

   if (req->type != EMPC_METADATA_TYPE_IMAGE) return EINA_FALSE;
   if (!req->album)
     {
        //if (eina_strlen_bounded(req->artist, 7) < 6)
          return EINA_FALSE; //too broad...
     }
   req->running = 1;

   if (req->artist)
     {
        sb = eina_strbuf_new();
        eina_strbuf_append(sb, req->artist);
        if (req->album)
          eina_strbuf_append_char(sb, '+');
        else
          eina_strbuf_replace_all(sb, " ", "+");
     }
   if (req->album)
     {
        if (!sb)
          sb = eina_strbuf_new();
        eina_strbuf_append(sb, req->album);
        eina_strbuf_replace_all(sb, " ", "+");
     }
   snprintf(buf, sizeof(buf), SEARCH_STR, eina_strbuf_string_get(sb));
   url = ecore_con_url_new(buf);
   ecore_con_url_additional_header_add(url, "user-agent", "Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0");
   ecore_con_url_get(url);
   gi = calloc(1, sizeof(GImage_Data));
   gi->req = (void*)req;
   gi->data = eina_strbuf_new();
   ecore_con_url_data_set(url, gi);
   reqs = eina_list_append(reqs, url);
   eina_strbuf_free(sb);
   return EINA_TRUE;
}

EAPI Eina_Bool
empc_module_remote(void)
{
   return EINA_TRUE;
}

EAPI Empc_Module_Type
empc_module_type(void)
{
   return EMPC_MODULE_TYPE_METADATA_FETCH;
}

EAPI int
empc_module_priority(void)
{
   return 20;
}

static Eina_Bool
google_image_init(void)
{
   if (!ecore_con_url_init()) return EINA_FALSE;
   ecore_init();
   E_LIST_HANDLER_APPEND(handlers, ECORE_CON_EVENT_URL_DATA, gdata, NULL);
   E_LIST_HANDLER_APPEND(handlers, ECORE_CON_EVENT_URL_COMPLETE, gdone, NULL);
   return EINA_TRUE;
}

static void
google_image_shutdown(void)
{
   E_FREE_LIST(handlers, ecore_event_handler_del);
   ecore_con_url_shutdown();
   ecore_shutdown();
}

EINA_MODULE_INIT(google_image_init);
EINA_MODULE_SHUTDOWN(google_image_shutdown);
