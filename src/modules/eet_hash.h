#include <Eet.h>

void eet_hash_init(void);
void eet_hash_shutdown(void);

void eet_hash_set(Eet_File *ef, const char *key, Eina_Stringshare *url);
Eina_Stringshare *eet_hash_get(Eet_File *ef, const char *key);
