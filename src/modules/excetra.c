#include "excetra.h"
#include <Evas.h>
#include <Ecore.h>
#include <Ecore_Con.h>
#include <Azy.h>
#include "Lyricwiki_Common_Azy.h"

#define DBG(...)            EINA_LOG_DOM_DBG(excetra_log_dom, __VA_ARGS__)
#define INF(...)            EINA_LOG_DOM_INFO(excetra_log_dom, __VA_ARGS__)
#define WRN(...)            EINA_LOG_DOM_WARN(excetra_log_dom, __VA_ARGS__)
#define ERR(...)            EINA_LOG_DOM_ERR(excetra_log_dom, __VA_ARGS__)
#define CRI(...)            EINA_LOG_DOM_CRIT(excetra_log_dom, __VA_ARGS__)

int excetra_log_dom = -1;

#define LYRICWIKI_SEARCH_TRACK_SIZE sizeof("http://lyrics.wikia.com/api.php?action=lyrics&artist=&song=&fmt=json")
#define LYRICWIKI_SEARCH_TRACK "http://lyrics.wikia.com/api.php?action=lyrics&artist="

struct Excetra_Req
{
   Ecore_Con_Url *url;
   Eina_Strbuf *buf;
   Excetra_Result_Cb cb;
   void *data;
   Eina_Bool instrumental : 1;
};

static Ecore_Event_Handler *handler_data;
static Ecore_Event_Handler *handler_done;
static Eina_List *reqs;

static void
excetra_req_free(Excetra_Req *req)
{
   reqs = eina_list_remove(reqs, req);
   eina_strbuf_free(req->buf);
   ecore_con_url_data_set(req->url, NULL);
   ecore_con_url_free(req->url);
   free(req);
}

static Eina_Bool
lyricwiki_data(void *d EINA_UNUSED, int type EINA_UNUSED, Ecore_Con_Event_Url_Data *ev)
{
   Excetra_Req *req = ecore_con_url_data_get(ev->url_con);

   if ((!reqs) || (!eina_list_data_find(reqs, req))) return ECORE_CALLBACK_RENEW;
   //DBG("Received %i bytes of lyric: %s", ev->size, ecore_con_url_url_get(ev->url_con));
   if (req->buf)
     eina_strbuf_append_length(req->buf, (char*)&ev->data[0], ev->size);
   else
     {
        req->buf = eina_strbuf_new();
        eina_strbuf_append_length(req->buf, (char*)&ev->data[7], ev->size - 7);
     }
   return ECORE_CALLBACK_DONE;
}

static char *
lyricwiki_parse_lyric(Eina_Strbuf *buf)
{
   size_t size;
   char *lyric_start, *lyric_end;
   const char *s;
   size = eina_strbuf_length_get(buf);
   if (!size)
     {
        ERR("size is 0!");
        return NULL;
     }
   s = eina_strbuf_string_get(buf);
#ifdef OLD_FETCH
   if (size > 40000) s += 40000;
   lyric_start = strstr(s, "phone_right.gif");
   if (lyric_start)
     {
        lyric_start = strstr(lyric_start, "</div>");
        if (!lyric_start) goto error;
        if (size - ((lyric_start - s) + sizeof("</div>") - 1) < 10) goto error;
        lyric_start += sizeof("</div>") - 1;
     }
   else
     {
        lyric_start = strstr(s, "<div class='lyricbox'>");
        if (!lyric_start) goto error;
        lyric_start = strstr(lyric_start, "</script>");
        if (!lyric_start) goto error;
        if (size - ((lyric_start - s) + sizeof("</script>") - 1) < 10) goto error;
        lyric_start += sizeof("</script>") - 1;
     }
   lyric_end = strstr(lyric_start, "<!--");
#else
   if (size > 2000) s += 2000;
   lyric_start = strstr(s, "&lt;lyrics&gt;\n");
   if (!lyric_start) goto error;
   lyric_start += sizeof("&lt;lyrics&gt;\n") - 1; //+\n
   lyric_end = strstr(lyric_start, "\n&lt;/lyrics&gt;");
#endif
   if (!lyric_end) goto error;
   lyric_end[0] = 0;
#ifdef OLD_FETCH
   while (lyric_end[-1] == '\n')
     {
        lyric_end[-1] = 0;
        lyric_end--;
     }
   return evas_textblock_text_markup_to_utf8(NULL, lyric_start);
#else
   return strdup(lyric_start);
#endif
error:
   ERR("Parsing error!");
   return NULL;
}

static void
lyricwiki_parse_stub(Excetra_Req *req)
{
   size_t size;
   Azy_Net *net;
   Azy_Content *content;
   Lyricwiki_Lyric *lwl;

   size = eina_strbuf_length_get(req->buf);
   DBG("\n%s", eina_strbuf_string_get(req->buf));
   if (!size)
     {
        ERR("size is 0!");
        return;
     }
   net = azy_net_buffer_new(eina_strbuf_string_steal(req->buf), size, AZY_NET_TRANSPORT_JSON, EINA_TRUE);
   content = azy_content_new(NULL);
   azy_content_deserialize(content, net);
   if (azy_value_to_Lyricwiki_Lyric(azy_content_retval_get(content), &lwl))
     {
        char buf[4096] = {0};
        const char *p;

        INF("STUB:");
        Lyricwiki_Lyric_print(NULL, 0, lwl);
        ecore_con_url_free(req->url);
        req->instrumental = lwl->lyrics && (!strcmp(lwl->lyrics, "Instrumental"));
        if (req->instrumental)
          {
             req->cb(req->data, req, NULL);
             excetra_req_free(req);
             return;
          }
#ifdef OLD_FETCH
        req->url = ecore_con_url_new(lwl->url);
#else
        p = strrchr(lwl->url, '/');
        memcpy(buf, lwl->url, p - lwl->url);
        memcpy(buf + (p - lwl->url), "/Special:Export", sizeof("/Special:Export") - 1);
        strncpy(buf + (p - lwl->url) + sizeof("/Special:Export") - 1, p, sizeof(buf) - ((p - lwl->url) + sizeof("/Special:Export") - 1));
        req->url = ecore_con_url_new(buf);
#endif
        ecore_con_url_data_set(req->url, req);
        ecore_con_url_get(req->url);
     }

   Lyricwiki_Lyric_free(lwl);
   azy_net_free(net);
   azy_content_free(content);
}

static Eina_Bool
lyricwiki_complete(void *d EINA_UNUSED, int type EINA_UNUSED, Ecore_Con_Event_Url_Complete *ev)
{
   const Eina_List *headers, *l;
   const char *h;
   char *lyric = NULL;
   Eina_Bool stub = EINA_TRUE;
   Azy_Net_Transport tp;
   Excetra_Req *req = ecore_con_url_data_get(ev->url_con);

   if ((!reqs) || (!eina_list_data_find(reqs, req))) return ECORE_CALLBACK_RENEW;
   DBG("%d code for lyrics: %s", ev->status, ecore_con_url_url_get(ev->url_con));
   headers = ecore_con_url_response_headers_get(ev->url_con);
   EINA_LIST_FOREACH(headers, l, h)
     {
        if (strncasecmp(h, "content-type: ", sizeof("content-type: ") - 1)) continue;
        h += sizeof("content-type: ") - 1;

        tp = azy_util_transport_get(h);
        if (tp == AZY_NET_TRANSPORT_JAVASCRIPT) break;
        if (tp == AZY_NET_TRANSPORT_HTML)
          {
             stub = EINA_FALSE;
             break;
          }
        
        req->cb(req->data, req, NULL);
        excetra_req_free(req);
        return ECORE_CALLBACK_DONE;
     }
   if (ev->status != 200)
     {
        req->cb(req->data, req, NULL);
        excetra_req_free(req);
        return ECORE_CALLBACK_DONE;
     }
   if (stub)
     lyricwiki_parse_stub(req);
   else
     {
        lyric = lyricwiki_parse_lyric(req->buf);
        req->cb(req->data, req, lyric);
        free(lyric);
        excetra_req_free(req);
     }
   return ECORE_CALLBACK_DONE;
}

void
excetra_req_cancel(Excetra_Req *req)
{
   if (req) excetra_req_free(req);
}

void *
excetra_req_data_get(const Excetra_Req *req)
{
   EINA_SAFETY_ON_NULL_RETURN_VAL(req, NULL);
   return req->data;
}

Excetra_Req *
excetra_req_new(const char *artist, const char *song, Excetra_Result_Cb cb, const void *data)
{
   Ecore_Con_Url *url;
   char buf[4096];
   char *a, *s, *p;
   Excetra_Req *req = NULL;

   if ((!artist) || (!artist[0]) || (!song) || (!song[0]) || (!cb)) return EINA_FALSE;

   a = strdup(artist);
   for (p = strchr(a, ' '); p; p = strchr(p + 1, ' '))
     *p = '_';
   s = strdup(song);
   for (p = strchr(s, ' '); p; p = strchr(p + 1, ' '))
     *p = '_';
   snprintf(buf, sizeof(buf), LYRICWIKI_SEARCH_TRACK "%s&song=%s&fmt=json", a, s);
   free(a);
   free(s);
   url = ecore_con_url_new(buf);
   if (ecore_con_url_get(url))
     {
        req = calloc(1, sizeof(Excetra_Req));
        req->url = url;
        req->cb = cb;
        req->data = (void*)data;
        reqs = eina_list_append(reqs, req);
        ecore_con_url_data_set(url, req);
     }
   else
     ecore_con_url_free(url);
   return req;
}

Eina_Bool
excetra_req_is_instrumental(const Excetra_Req *req)
{
   return req->instrumental;
}

int
excetra_init(void)
{
   eina_init();
   ecore_init();
   excetra_log_dom = eina_log_domain_register("excetra", EINA_COLOR_CYAN);
   eina_log_domain_level_set("excetra", EINA_LOG_LEVEL_DBG);
   if (excetra_log_dom == -1)
     {
        fprintf(stderr, "Could not init log domain!\n");
        return 0;
     }
   if (!ecore_con_url_init())
     {
        ERR("CURL support is required!");
        return 0;
     }
   evas_init();
   azy_init();
   handler_done = ecore_event_handler_add(ECORE_CON_EVENT_URL_COMPLETE, (Ecore_Event_Handler_Cb)lyricwiki_complete, NULL);
   handler_data = ecore_event_handler_add(ECORE_CON_EVENT_URL_DATA, (Ecore_Event_Handler_Cb)lyricwiki_data, NULL);
   return 1;
}

void
excetra_shutdown(void)
{
   ecore_event_handler_del(handler_done);
   handler_done = NULL;
   ecore_event_handler_del(handler_data);
   handler_data = NULL;
   azy_shutdown();
   evas_shutdown();
   ecore_con_url_shutdown();
   eina_log_domain_unregister(excetra_log_dom);
   excetra_log_dom = -1;
   ecore_shutdown();
   eina_shutdown();
}

#ifndef EXCETRA_LIB
static void
result_cb(void *data EINA_UNUSED, Excetra_Req *req, const char *lyric)
{
   INF("\n%s", excetra_req_is_instrumental(req) ? "Instrumental" : lyric);
   ecore_main_loop_quit();
}

int
main(int argc, char *argv[])
{
   if (argc != 3)
     {
        fprintf(stderr, "USAGE: %s ARTIST SONG\n", argv[0]);
        exit(1);
     }
   if (!excetra_init()) exit(1);
   excetra_req_new(argv[1], argv[2], result_cb, NULL);
   ecore_main_loop_begin();
   return 0;
}
#endif
