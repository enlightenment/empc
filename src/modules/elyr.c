#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "empdd.h"
#include "empc.h"
#include <Ecore.h>
#include <Efreet.h>
#include <Elementary.h>
#define EXCETRA_LIB
#include "excetra.h"

typedef struct Elyr_Req
{
   Empc_Fetch_Request *req;
   Eina_List *ereqs;
} Elyr_Req;

static Eina_List *reqs = NULL;
static void result_cb(Elyr_Req *eq, Excetra_Req *ereq, const char *lyric);

static void
elyr_req_free(Elyr_Req *eq)
{
   E_FREE_LIST(eq->ereqs, excetra_req_cancel);
   free(eq);
}

static void
result_cb(Elyr_Req *eq, Excetra_Req *ereq, const char *lyric)
{
   Evas_Object *o;
   Eina_List *l, *ll;
   char *txt;

   eq->ereqs = eina_list_remove(eq->ereqs, ereq);
   if (!lyric)
     {
        if (excetra_req_is_instrumental(ereq) || (!eq->ereqs))
          {
             empc_metadata_fetch_done(eq->req, NULL);
             reqs = eina_list_remove(reqs, eq);
             elyr_req_free(eq);
          }
        return;
     }
   l = empc_metadata_entries_add(eq->req);
   txt = elm_entry_utf8_to_markup(lyric);
   EINA_LIST_FOREACH(l, ll, o)
     elm_entry_entry_set(o, txt);
   free(txt);
   empc_metadata_fetch_done(eq->req, l);
   if (eq->ereqs && (!eq->req->force)) return;
   empc_metadata_fetch_done(eq->req, NULL);
   reqs = eina_list_remove(reqs, eq);
   elyr_req_free(eq);
}

EAPI void
empc_module_metadata_cancel(const Empc_Fetch_Request *req)
{
   Eina_List *l, *ll;
   Excetra_Req *ereq;

   EINA_LIST_FOREACH_SAFE(reqs, l, ll, ereq)
     {
        if (excetra_req_data_get(ereq) != req) continue;
        reqs = eina_list_remove_list(reqs, l);
        excetra_req_cancel(ereq);
        break;
     }
}

EAPI Eina_Bool
empc_module_metadata_fetch(const Empc_Fetch_Request *req)
{
   Excetra_Req *ereq;
   Elyr_Req *eq;
   const char *p;
   char *s, *song;

   if (req->type != EMPC_METADATA_TYPE_TEXT) return EINA_FALSE;
   eq = calloc(1, sizeof(Elyr_Req));
   ereq = excetra_req_new(req->artist, req->song, (Excetra_Result_Cb)result_cb, eq);
   if (!ereq)
     {
        free(eq);
        return EINA_FALSE;
     }
   eq->req = (Empc_Fetch_Request*)req;
   eq->ereqs = eina_list_append(eq->ereqs, ereq);

   /* lyricwiki's api is fucking terrible and has no ability to do fuzzy matching
    * because of this, it is frequently the case that song titles which include '-'
    * will match wrong due to the lack of surrounding spaces.
    * in this block, I'll attempt the shotgun approach and try fetching with sequential
    * '-' escapes
    */
   s = song = calloc(1, (strlen(req->song) * 3) + 1);
   for (p = req->song; p[0]; p++, s++)
     {
        Eina_Bool rep = EINA_FALSE;

        if ((p - req->song > 0) && (p[0] == '-') && (p[-1] != ' ') && (p[-1] != '_'))
          {
             s[0] = '_';
             s++;
             rep = EINA_TRUE;
          }
        s[0] = p[0];
        if (p[0] == '-')
          {

             if ((p[1] != ' ') && (p[1] != '_'))
               {
                  rep = EINA_TRUE;
                  s++;
                  s[0] = '_';
               }
             if (!rep) continue;
             strcpy(s + 1, p + 1);
             ereq = excetra_req_new(req->artist, song, (Excetra_Result_Cb)result_cb, eq);
             if (ereq)
               eq->ereqs = eina_list_append(eq->ereqs, ereq);
          }
     }
   free(song);
   
   return EINA_TRUE;
}

EAPI Eina_Bool
empc_module_remote(void)
{
   return EINA_TRUE;
}

EAPI Empc_Module_Type
empc_module_type(void)
{
   return EMPC_MODULE_TYPE_METADATA_FETCH;
}

EAPI int
empc_module_priority(void)
{
   return 25;
}

static Eina_Bool
elyr_init(void)
{
   excetra_init();
   eina_log_domain_level_set("excetra", EINA_LOG_LEVEL_ERR);
   return EINA_TRUE;
}

static void
elyr_shutdown(void)
{
   E_FREE_LIST(reqs, elyr_req_free);
   excetra_shutdown();
}

EINA_MODULE_INIT(elyr_init);
EINA_MODULE_SHUTDOWN(elyr_shutdown);
