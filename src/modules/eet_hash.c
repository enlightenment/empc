#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <Ecore.h>
#include <math.h>
#include <Eet.h>

static Eet_Data_Descriptor *urls = NULL;
static Eet_Data_Descriptor *url_info = NULL;

typedef struct Urls
{
   Eina_Hash *urls;
} Urls;

typedef struct Url_Info
{
   unsigned long long last_used;
   Eina_Stringshare *url;
} Url_Info;

static void
ui_free(Url_Info *ui)
{
   if (!ui) return;
   eina_stringshare_del(ui->url);
   free(ui);
}

void
eet_hash_set(Eet_File *ef, const char *key, Eina_Stringshare *url)
{
   Urls *u;
   Url_Info *ui;

   u = eet_data_read(ef, urls, "hash");
   ui = malloc(sizeof(Url_Info));
   ui->last_used = lround(ecore_time_unix_get());
   ui->url = eina_stringshare_add(url);
   if (!u)
     {
        u = malloc(sizeof(Urls));
        u->urls = eina_hash_string_superfast_new((Eina_Free_Cb)ui_free);
     }
   ui_free(eina_hash_set(u->urls, key, ui));
   eet_data_write(ef, urls, "hash", u, 1);
   eina_hash_free(u->urls);
   free(u);
}

Eina_Stringshare *
eet_hash_get(Eet_File *ef, const char *key)
{
   Urls *u;
   Url_Info *ui;
   Eina_Stringshare *ret = NULL;

   u = eet_data_read(ef, urls, "hash");
   if (!u) return NULL;
   eina_hash_free_cb_set(u->urls, (Eina_Free_Cb)ui_free);
   ui = eina_hash_set(u->urls, key, NULL);
   if (ui)
     ret = ui->url;
   free(ui);
   eina_hash_free(u->urls);
   free(u);
   return ret;
}

void
eet_hash_init(void)
{
   Eet_Data_Descriptor_Class eddc;

   if (urls && url_info) return;
   eet_init();
   eet_eina_stream_data_descriptor_class_set(&eddc, sizeof(eddc), "Urls", sizeof(Urls));
   urls = eet_data_descriptor_stream_new(&eddc);

   eet_eina_stream_data_descriptor_class_set(&eddc, sizeof(eddc), "Url_Info", sizeof(Url_Info));
   url_info = eet_data_descriptor_stream_new(&eddc);
   EET_DATA_DESCRIPTOR_ADD_BASIC(url_info, Url_Info, "last_used", last_used, EET_T_ULONG_LONG);
   EET_DATA_DESCRIPTOR_ADD_BASIC(url_info, Url_Info, "url", url, EET_T_INLINED_STRING);


   EET_DATA_DESCRIPTOR_ADD_HASH(urls, Urls, "Urls", urls, url_info);
}

void
eet_hash_shutdown(void)
{
   if (!urls) return;
   eet_shutdown();
   eet_data_descriptor_free(url_info);
   eet_data_descriptor_free(urls);
   url_info = urls = NULL;
}
