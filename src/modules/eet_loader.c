#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "empdd.h"
#include "empc.h"
#include "eet_hash.h"
#include <Ecore.h>
#include <Efreet.h>
#include <Elementary.h>
#include <Eet.h>

static Eet_File *ef_img = NULL;
static Eet_File *ef_lyr = NULL;

static Eina_Hash *jobs = NULL;
static Eina_Hash *downloads = NULL;

static Eina_Bool
try_lyr(void)
{
   char buf[PATH_MAX];

   snprintf(buf, sizeof(buf), "%s/empc/metadata/lyrics.eet", efreet_cache_home_get());
   ef_lyr = eet_open(buf, EET_FILE_MODE_READ);
   return !!ef_lyr;
}

static Eina_Bool
try_img(void)
{
   char buf[PATH_MAX];

   snprintf(buf, sizeof(buf), "%s/empc/metadata/images.eet", efreet_cache_home_get());
   ef_img = eet_open(buf, EET_FILE_MODE_READ);
   return !!ef_img;
}

static void
download_del(void *data, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   empc_metadata_fetch_done(data, NULL);
}

static void
download_start(void *data EINA_UNUSED, Evas_Object *obj, void *event_info EINA_UNUSED)
{
   evas_object_data_set(obj, "__empc_downloading", (void*)1);
}

static void
download_done(void *data, Evas_Object *obj, void *event_info EINA_UNUSED)
{
   Empc_Fetch_Request *req = data;
   Eina_List *l, *ll;
   Evas_Object *o, *img;
   const Eina_File *m;

   evas_object_event_callback_del_full(obj, EVAS_CALLBACK_DEL, download_del, data);
   evas_object_del(eina_hash_set(downloads, &data, NULL));
   evas_object_data_del(obj, "__empc_downloading");
   l = empc_metadata_images_add(req);
   img = elm_image_object_get(obj);
   evas_object_image_mmap_get(img, &m, NULL);
   evas_object_data_set(l->data, "__empc_resave", (void*)1);
   EINA_LIST_FOREACH(l, ll, o)
     elm_image_mmap_set(o, m, NULL);
   evas_object_del(obj);
   empc_metadata_fetch_done(data, l);
}

static void
download_error(void *data, Evas_Object *obj, void *event_info EINA_UNUSED)
{
   evas_object_del(eina_hash_set(downloads, &data, NULL));
   evas_object_data_del(obj, "__empc_downloading");
   evas_object_del(obj);
}

static void
_loader_fetch(Empc_Fetch_Request *req)
{
   char buf[4096];
   char *a = NULL, *b = NULL, *c = NULL;
   int num = 0;
   Evas_Object *o = NULL;
   Eet_File *ef = NULL;
   Eina_List *l = NULL, *ll;

   ecore_job_del(eina_hash_set(jobs, &req, NULL));
   if (req->type == EMPC_METADATA_TYPE_IMAGE)
     {
        if (!try_img())
          goto end;
        ef = ef_img;
     }
   else if (req->type == EMPC_METADATA_TYPE_TEXT)
     {
        if (!try_lyr())
          goto end;
        ef = ef_lyr;
     }
   if (!ef) goto end;
   if (req->artist)
     {
        a = strdupa(req->artist);
        eina_str_tolower(&a);
     }
   if (req->album)
     {
        b = strdupa(req->album);
        eina_str_tolower(&b);
     }
   if (req->song)
     {
        c = strdupa(req->song);
        eina_str_tolower(&c);
     }
   if (req->type == EMPC_METADATA_TYPE_IMAGE)
     snprintf(buf, sizeof(buf), "%s:::%s", a ?: "", b ?: "");
   else
     snprintf(buf, sizeof(buf), "%s:::%s", a ?: "", c ?: "");
   free(eet_list(ef, buf, &num));
   if (req->type == EMPC_METADATA_TYPE_IMAGE)
     {
        if (num)
          {
             Eina_Bool fail = EINA_FALSE, uri = EINA_FALSE;

             l = empc_metadata_images_add(req);
             EINA_LIST_FOREACH(l, ll, o)
               {
                  const char *f, *g;
                  if (uri)
                    f = buf, g = NULL;
                  else
                    f = eet_file_get(ef_img), g = buf;
                  if (elm_image_file_set(o, f, g))
                    {
                       if (!uri)
                         evas_object_data_set(o, "__empc_nosave", (void*)1);
                    }
                  else
                    {
                       if (!uri)
                         {
                            void *data;

                            data = eet_read(ef_img, buf, &num);
                            if (data && (num < (int)sizeof(buf) - 1)) //possibly a URI
                              {
                                 memcpy(buf, data, num);
                                 buf[num] = 0;
                                 evas_object_smart_callback_add(o, "download,start", download_start, req);
                                 evas_object_smart_callback_add(o, "download,done", download_done, req);
                                 evas_object_smart_callback_add(o, "download,error", download_error, req);
                                 if (elm_image_file_set(o, buf, NULL))
                                   {
                                      evas_object_event_callback_add(o, EVAS_CALLBACK_DEL, download_del, req);
                                      eina_hash_add(downloads, &req, o);
                                      l = eina_list_remove_list(l, l);
                                      E_FREE_LIST(l, evas_object_del);
                                      eet_close(ef_img);
                                      ef_img = NULL;
                                      return;
                                   }
                              }
                         }
                       fail = EINA_TRUE;
                       break;
                    }
               }
             if (fail)
               E_FREE_LIST(l, evas_object_del);
          }
        else
          {
             Eina_Stringshare *url;

             url = eet_hash_get(ef_img, buf);
             if (url)
               {
                  if (url[0] == '/')
                    {
                       l = empc_metadata_images_add(req);
                       EINA_LIST_FOREACH(l, ll, o)
                         {
                            if (elm_image_file_set(o, url, NULL))
                              evas_object_data_set(o, "__empc_nosave", (void*)1);
                            else
                              {
                                 E_FREE_LIST(l, evas_object_del);
                                 break;
                              }
                         }
                    }
                  else
                    {
                       if (req->force)
                         empc_metadata_image_download(req, url);
                       else
                         empc_metadata_image_download_queue(req, url);
                    }
                  eina_stringshare_del(url);
               }
          }
        eet_close(ef_img);
        ef_img = NULL;
     }
   else
     {
        void *txt;

        if (num)
          {
             txt = eet_read(ef_lyr, buf, &num);
             l = empc_metadata_entries_add(req);
             EINA_LIST_FOREACH(l, ll, o)
               {
                  evas_object_data_set(o, "__empc_nosave", (void*)1);
                  elm_entry_entry_set(o, txt);
               }
             free(txt);
          }
        eet_close(ef_lyr);
        ef_lyr = NULL;
     }
   if (l)
     empc_metadata_fetch_done(req, l);
end:
   empc_metadata_fetch_done(req, NULL);
}

EAPI void
empc_module_metadata_cancel(const Empc_Fetch_Request *req)
{
   evas_object_del(eina_hash_set(downloads, &req, NULL));
   ecore_job_del(eina_hash_set(jobs, &req, NULL));
}

EAPI Eina_Bool
empc_module_metadata_fetch(const Empc_Fetch_Request *req)
{
   return eina_hash_add(jobs, &req, ecore_job_add((Ecore_Cb)_loader_fetch, (void*)req));
}

EAPI Empc_Module_Type
empc_module_type(void)
{
   return EMPC_MODULE_TYPE_METADATA_FETCH;
}

EAPI int
empc_module_priority(void)
{
   return 5;
}

static Eina_Bool
eet_loader_init(void)
{
   eet_init();
   eet_hash_init();
   jobs = eina_hash_pointer_new(NULL);
   downloads = eina_hash_pointer_new(NULL);

   return EINA_TRUE;
}

static void
eet_loader_shutdown(void)
{
   if (ef_img) eet_close(ef_img);
   if (ef_lyr) eet_close(ef_lyr);
   eina_hash_free(jobs);
   eina_hash_free(downloads);
   eet_hash_shutdown();
   eet_shutdown();
}

EINA_MODULE_INIT(eet_loader_init);
EINA_MODULE_SHUTDOWN(eet_loader_shutdown);
