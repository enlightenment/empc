#include <Eina.h>

typedef struct Excetra_Req Excetra_Req;

typedef void (*Excetra_Result_Cb)(void *user_data, Excetra_Req *req, const char *lyrics);

EAPI int excetra_init(void);
EAPI void excetra_shutdown(void);
EAPI Excetra_Req *excetra_req_new(const char *artist, const char *song, Excetra_Result_Cb cb, const void *data);
EAPI void excetra_req_cancel(Excetra_Req *req);
EAPI void *excetra_req_data_get(const Excetra_Req *req);
EAPI Eina_Bool excetra_req_is_instrumental(const Excetra_Req *req);
