#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "empdd.h"
#include "empc.h"
#include <Esskyuehl.h>
#include <Ecore.h>
#include <Efreet.h>
#include <Elementary.h>

typedef enum
{
   GLYR_TYPE_UNKNOWN,
   GLYR_TYPE_LYRICS,
   GLYR_TYPE_ALBUM_REVIEW,
   GLYR_TYPE_ARTIST_PHOTO,
   GLYR_TYPE_COVERART,
   GLYR_TYPE_ARTIST_BIO,
   GLYR_TYPE_SIMILAR_ARTIST,
   GLYR_TYPE_SIMILAR_SONG,
   GLYR_TYPE_ALBUMLIST,
   GLYR_TYPE_TAG,
   GLYR_TYPE_TAG_ARTIST,
   GLYR_TYPE_TAG_ALBUM,
   GLYR_TYPE_TAG_TITLE,
   GLYR_TYPE_RELATION,
   GLYR_TYPE_IMG_URL,
   GLYR_TYPE_TXT_URL,
   GLYR_TYPE_TRACK,
   GLYR_TYPE_GUITARTABS,
   GLYR_TYPE_BACKDROPS
} GLYR_DATA_TYPE;

typedef enum
{
   GLYR_GET_UNKNOWN,
   GLYR_GET_COVERART,
   GLYR_GET_LYRICS,
   GLYR_GET_ARTIST_PHOTOS,
   GLYR_GET_ARTIST_BIO,
   GLYR_GET_SIMILAR_ARTISTS,
   GLYR_GET_SIMILAR_SONGS,
   GLYR_GET_ALBUM_REVIEW,
   GLYR_GET_TRACKLIST,
   GLYR_GET_TAGS,
   GLYR_GET_RELATIONS,
   GLYR_GET_ALBUMLIST,
   GLYR_GET_GUITARTABS,
   GLYR_GET_BACKDROPS,
   GLYR_GET_ANY
} GLYR_GET_TYPE;

static Eina_List *handlers = NULL;
static Esql *e = NULL;
static Eina_Bool connected = EINA_FALSE;

/* Jesus H.R. Puffnstuf Christ */
static const char glyr_query[] =
{
    "SELECT artist_name,                                      "
    "        album_name,                                      "
    "        title_name,                                      "
    "        source_url,                                      "
    "        data_type,                                       "
    "        data_size,                                       "
    "        data_is_image,                                   "
    "        data                                             "
    "FROM metadata as m                                       "
    "LEFT JOIN artists AS a ON m.artist_id  = a.rowid         "
    "LEFT JOIN albums  AS b ON m.album_id   = b.rowid         "
    "LEFT JOIN titles  AS t ON m.title_id   = t.rowid         "
    "LEFT JOIN image_types as i on m.image_type_id = i.rowid  "
    "WHERE m.get_type = %d                                    "
    "                   %s "
    "                   %s "
    "                   %s "
    "                   %s                                    "
    "LIMIT %d;                                                "
};

static Eina_Bool
_connect(void *d EINA_UNUSED, int t EINA_UNUSED, Esql *ev EINA_UNUSED)
{
   connected = EINA_TRUE;
   return ECORE_CALLBACK_CANCEL;
}

static void
glyr_gmpc_result_cb(Esql_Res *res, Empc_Fetch_Request *req)
{
   Eina_Iterator *it;
   Esql_Row *row;

   it = esql_res_row_iterator_new(res);
   EINA_ITERATOR_FOREACH(it, row)
     {
        //const char *artist = NULL;
        //const char *album = NULL;
        const char *source_url = NULL;
        Eina_Value tmp;
        const Eina_Value *val;
        Eina_Value_Blob blob = { .size = 0, .memory = NULL };

        val = esql_row_value_struct_get(row);
        //eina_value_struct_get(val, "artist_name", &artist);
        eina_value_struct_get(val, "source_url", &source_url);
        //if (req->album)
          //eina_value_struct_get(val, "album_name", &album);
        if (eina_value_struct_value_get(val, "data", &tmp))
          {
             if (eina_value_pget(&tmp, &blob))
               {
                  Evas_Object *o;
                  Eina_List *l, *ll, *lll;
                  Eina_File *f;
                  char buf[1024];

                  snprintf(buf, sizeof(buf), "%s::%s", req->artist, req->album);
                  f = eina_file_virtualize(buf, blob.memory, blob.size, EINA_FALSE);
                  l = empc_metadata_images_add(req);
                  EINA_LIST_FOREACH_SAFE(l, ll, lll, o)
                    {
                       if (!elm_image_mmap_set(o, f, NULL))
                         {
                            evas_object_del(o);
                            l = eina_list_remove_list(l, ll);
                         }
                    }
                  if (l)
                    empc_metadata_fetch_done(req, l);
                  eina_file_close(f);
                  eina_value_flush(&tmp);
                  break;
               }
          }
     }
   eina_iterator_free(it);
   empc_metadata_fetch_done(req, NULL);
}

EAPI Eina_Bool
empc_module_metadata_fetch(const Empc_Fetch_Request *req)
{
   Esql_Query_Id id;
   char artist[128] = {0}, album[128] = {0};
   char buf[2048];
   char *a, *al;
   

   if ((!connected) || (req->type != EMPC_METADATA_TYPE_IMAGE)) return EINA_FALSE;
   a = esql_string_escape(0, req->artist);
   eina_str_tolower(&a);
   snprintf(artist, sizeof(artist), "AND artist_name = '%s'", a);
   free(a);
   if (req->album)
     {
        al = esql_string_escape(0, req->album);
        eina_str_tolower(&al);
        snprintf(album, sizeof(album), "AND album_name = '%s'", al);
        free(al);
     }
   snprintf(buf, sizeof(buf), glyr_query,
            req->album ? GLYR_GET_COVERART : GLYR_GET_ARTIST_PHOTOS,
            "",
            album,
            artist,
            "AND NOT data_type = 14",
            1);
        
   id = esql_query(e, (void*)req, buf);
   if (id)
     esql_query_callback_set(id, (Esql_Query_Cb)glyr_gmpc_result_cb);
   return !!id;
}

EAPI Empc_Module_Type
empc_module_type(void)
{
   return EMPC_MODULE_TYPE_METADATA_FETCH;
}

EAPI int
empc_module_priority(void)
{
   return 45;
}

static Eina_Bool
glyr_gmpc_init(void)
{
   char buf[PATH_MAX];

   ecore_init();
   efreet_init();
   snprintf(buf, sizeof(buf), "%s/gmpc/metadata/metadata.db", efreet_cache_home_get());
   esql_init();

   E_LIST_HANDLER_APPEND(handlers, ESQL_EVENT_CONNECT, _connect, NULL);
   
   e = esql_new(ESQL_TYPE_SQLITE);
   return esql_connect(e, buf, NULL, NULL);
}

static void
glyr_gmpc_shutdown(void)
{
   esql_free(e);
   E_FREE_LIST(handlers, ecore_event_handler_del);
   esql_shutdown();
   efreet_shutdown();
   ecore_main_loop_iterate();
   ecore_shutdown();
}

EINA_MODULE_INIT(glyr_gmpc_init);
EINA_MODULE_SHUTDOWN(glyr_gmpc_shutdown);
