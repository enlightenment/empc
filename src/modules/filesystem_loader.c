#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "empdd.h"
#include "empc.h"
#include <Ecore.h>
#include <Efreet.h>
#include <Elementary.h>
#include <sys/stat.h>

typedef struct FSInfo
{
   char *uri;
   Empc_Fetch_Request *req;
} FSInfo;

static const char *suffixes[] =
{
   ".jpg",
   ".jpeg",
   ".png",
   ".gif",
   NULL
};

static Eina_Bool
suffix_check(const char *uri)
{
   const char **suffix;

   for (suffix = suffixes; *suffix; suffix++)
     if (eina_str_has_extension(uri, *suffix)) return EINA_TRUE;
   return EINA_FALSE;
}

static void
fsnotify(FSInfo *fsi, Ecore_Thread *eth EINA_UNUSED, Eina_List *files)
{
   char *img;

   EINA_LIST_FREE(files, img)
     {
        Eina_List *l, *ll;
        Eina_Bool fail = EINA_FALSE;
        Evas_Object *o;

        l = empc_metadata_images_add(fsi->req);
        EINA_LIST_FOREACH(l, ll, o)
          {
             evas_object_data_set(o, "__empc_nosave", (void*)1);
             evas_object_data_set(o, "__empc_override", (void*)1);
             if (!elm_image_file_set(o, img, NULL))
               {
                  fail = EINA_TRUE;
                  break;
               }
          }
        if (fail)
          E_FREE_LIST(l, evas_object_del);
        if (l)
          empc_metadata_fetch_done(fsi->req, l);
        free(img);
     }
}

static void
fsend(FSInfo *fsi, Ecore_Thread *eth EINA_UNUSED)
{
   fsi->req->running = 0;
   empc_metadata_fetch_done(fsi->req, NULL);
   free(fsi->uri);
   free(fsi);
}

static Eina_List *
fsscan(Eina_Iterator *it, Eina_List *files, Eina_Bool recursive)
{
   Eina_File_Direct_Info *info;

   EINA_ITERATOR_FOREACH(it, info)
     {
        if (info->type != EINA_FILE_REG)
          {
             Eina_Iterator *it2;

             if ((!recursive) || (info->type != EINA_FILE_DIR))
               continue;
             it2 = eina_file_stat_ls(info->path);
             files = fsscan(it2, files, recursive);
          }
        if (!suffix_check(info->path)) continue;
        files = eina_list_append(files, strdup(info->path));
     }
   return files;
}

static void
fsrun(FSInfo *fsi, Ecore_Thread *eth)
{
   
   Eina_Iterator *it;
   struct stat st;
   char *file, buf[PATH_MAX] = {0};
   Eina_Bool isdir;
   Eina_List *l, *ll, *final = NULL, *files = NULL;
   unsigned int i;
   const char *special[] =
   {
      "cover",
      "folder",
      "front"
   };
   Eina_List *s[EINA_C_ARRAY_LENGTH(special)] = {NULL};

   if (stat(fsi->uri, &st)) return;
   isdir = S_ISDIR(st.st_mode);
   if (!isdir)
     {
        const char *p;

        p = strrchr(fsi->uri, '/');
        memcpy(buf, fsi->uri, MIN((unsigned int)(p - fsi->uri), sizeof(buf) - 1));
     }

   it = eina_file_stat_ls(isdir ? fsi->uri : buf);
   files = fsscan(it, files, !!fsi->req->album);
   eina_iterator_free(it);
   if ((!files) && (!isdir))
     {
        char *p;

        p = strrchr(buf, '/');
        if (p)
          {
             p[0] = 0;
             if (strstr(buf, fsi->req->album ?: fsi->req->artist))
               {
                  it = eina_file_stat_ls(buf);
                  files = fsscan(it, files, !!fsi->req->album);
               }
          }
     }
   EINA_LIST_FOREACH_SAFE(files, l, ll, file)
     {
        char *p;

        p = strrchr(file, '/');
        if (p) p++;
        else
          p = file;
        for (i = 0; i < EINA_C_ARRAY_LENGTH(special); i++)
          if (!strncasecmp(p, special[i], strlen(special[i])))
            {
               s[i] = eina_list_append(s[i], file);
               files = eina_list_remove_list(files, l);
            }
     }
   for (i = 0; i < EINA_C_ARRAY_LENGTH(special); i++)
     if (s[i]) final = eina_list_merge(final, s[i]);
   if (files)
     final = eina_list_merge(final, files);
   ecore_thread_feedback(eth, final);
}

EAPI Eina_Bool
empc_module_metadata_fetch(Empc_Fetch_Request *req)
{
   char buf[PATH_MAX];
   FSInfo *fsi;

   if (req->type != EMPC_METADATA_TYPE_IMAGE) return EINA_FALSE;
   if ((!req->uri) || (!empd_music_directory)) return EINA_FALSE;
   req->running = 1;

   snprintf(buf, sizeof(buf), "%s/%s", empd_music_directory, req->uri);
   fsi = malloc(sizeof(FSInfo));
   fsi->uri = strdup(buf);
   fsi->req = req;
   ecore_thread_feedback_run((Ecore_Thread_Cb)fsrun, (Ecore_Thread_Notify_Cb)fsnotify, (Ecore_Thread_Cb)fsend, (Ecore_Thread_Cb)fsend, fsi, 0);
   return EINA_TRUE;
}

EAPI Empc_Module_Type
empc_module_type(void)
{
   return EMPC_MODULE_TYPE_METADATA_FETCH;
}

EAPI int
empc_module_priority(void)
{
   return 10;
}

static Eina_Bool
fs_init(void)
{
   return EINA_TRUE;
}

static void
fs_shutdown(void)
{
}

EINA_MODULE_INIT(fs_init);
EINA_MODULE_SHUTDOWN(fs_shutdown);
