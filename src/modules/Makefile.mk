mod_cppflags = \
-DPACKAGE_DATA_DIR=\"$(datadir)/empc\"

moddir = $(libdir)/empc/$(MODULE_ARCH)

mod_LTLIBRARIES =

if MOD_GLYR_GMPC
mod_LTLIBRARIES += src/modules/glyr_gmpc.la

src_modules_glyr_gmpc_la_SOURCES = \
src/modules/glyr_gmpc.c \
src/bin/empc.h

src_modules_glyr_gmpc_la_CPPFLAGS = \
$(AM_CFLAGS) \
$(mod_cppflags) \
@EFL_CFLAGS@ \
@ELM_CFLAGS@ \
@ESQL_CFLAGS@ \
-I$(top_srcdir)/src/bin \
-I$(top_builddir)

src_modules_glyr_gmpc_la_LIBADD = \
@EFL_LIBS@ \
@ELM_LIBS@ \
@ESQL_LIBS@

src_modules_glyr_gmpc_la_LDFLAGS = -module -avoid-version

src_modules_glyr_gmpc_la_LIBTOOLFLAGS = --tag=disable-static
endif


if MOD_GLYR
mod_LTLIBRARIES += src/modules/glyr.la

src_modules_glyr_la_SOURCES = \
src/modules/glyr.c \
src/bin/empc.h

src_modules_glyr_la_CPPFLAGS = \
$(AM_CFLAGS) \
$(mod_cppflags) \
@EFL_CFLAGS@ \
@ELM_CFLAGS@ \
-I$(top_srcdir)/src/bin \
-I$(top_builddir)

src_modules_glyr_la_LIBADD = \
@EFL_LIBS@ \
@ELM_LIBS@

src_modules_glyr_la_LDFLAGS = -module -avoid-version

src_modules_glyr_la_LIBTOOLFLAGS = --tag=disable-static
endif

if MOD_GOOGLE_IMAGE
mod_LTLIBRARIES += src/modules/google_image.la

src_modules_google_image_la_SOURCES = \
src/modules/google_image.c \
src/bin/empc.h

src_modules_google_image_la_CPPFLAGS = \
$(AM_CFLAGS) \
$(mod_cppflags) \
@EFL_CFLAGS@ \
@ELM_CFLAGS@ \
-I$(top_srcdir)/src/bin \
-I$(top_builddir)

src_modules_google_image_la_LIBADD = \
@EFL_LIBS@ \
@ELM_LIBS@

src_modules_google_image_la_LDFLAGS = -module -avoid-version

src_modules_google_image_la_LIBTOOLFLAGS = --tag=disable-static
endif

if MOD_ELYR
ELYR_AZY_SRC = \
src/modules/Lyricwiki_Common_Azy.c \
src/modules/Lyricwiki_Common_Azy.h \
src/modules/Lyricwiki_Common.c \
src/modules/Lyricwiki_Common.h

MAINTAINERCLEANFILES += $(ELYR_AZY_SRC)
EXTRA_DIST += \
src/modules/excetra.azy \
$(ELYR_AZY_SRC)

$(ELYR_AZY_SRC): src/modules/excetra.azy
	@AZY_PARSER@ -H -o $(top_builddir)/src/modules src/modules/excetra.azy

mod_LTLIBRARIES += src/modules/elyr.la

src_modules_elyr_la_SOURCES = \
src/modules/elyr.c \
src/bin/empc.h \
src/modules/excetra.c \
src/modules/excetra.h \
$(ELYR_AZY_SRC)

src/modules/excetra.c: $(ELYR_AZY_SRC)

src_modules_elyr_la_CPPFLAGS = \
$(AM_CFLAGS) \
$(mod_cppflags) \
@EFL_CFLAGS@ \
@ELM_CFLAGS@ \
@AZY_CFLAGS@ \
-I$(top_srcdir)/src/bin \
-I$(top_builddir)

src_modules_elyr_la_LIBADD = \
@AZY_LIBS@ \
@EFL_LIBS@ \
@ELM_LIBS@

src_modules_elyr_la_LDFLAGS = -module -avoid-version

src_modules_elyr_la_LIBTOOLFLAGS = --tag=disable-static
endif


if MOD_EET_SAVER
mod_LTLIBRARIES += src/modules/eet_saver.la

src_modules_eet_saver_la_SOURCES = \
src/modules/eet_saver.c \
src/modules/eet_hash.c \
src/bin/empc.h

src_modules_eet_saver_la_CPPFLAGS = \
$(AM_CFLAGS) \
$(mod_cppflags) \
@EFL_CFLAGS@ \
@ELM_CFLAGS@ \
-I$(top_srcdir)/src/bin \
-I$(top_builddir)

src_modules_eet_saver_la_LIBADD = \
@EFL_LIBS@ \
@EIO_LIBS@ \
@ELM_LIBS@

src_modules_eet_saver_la_LDFLAGS = -module -avoid-version

src_modules_eet_saver_la_LIBTOOLFLAGS = --tag=disable-static
endif


if MOD_EET_LOADER
mod_LTLIBRARIES += src/modules/eet_loader.la

src_modules_eet_loader_la_SOURCES = \
src/modules/eet_loader.c \
src/modules/eet_hash.c \
src/bin/empc.h

src_modules_eet_loader_la_CPPFLAGS = \
$(AM_CFLAGS) \
$(mod_cppflags) \
@EFL_CFLAGS@ \
@ELM_CFLAGS@ \
-I$(top_srcdir)/src/bin \
-I$(top_builddir)

src_modules_eet_loader_la_LIBADD = \
@EFL_LIBS@ \
@ELM_LIBS@

src_modules_eet_loader_la_LDFLAGS = -module -avoid-version

src_modules_eet_loader_la_LIBTOOLFLAGS = --tag=disable-static
endif

if MOD_FILESYSTEM_LOADER
mod_LTLIBRARIES += src/modules/filesystem_loader.la

src_modules_filesystem_loader_la_SOURCES = \
src/modules/filesystem_loader.c \
src/bin/empc.h

src_modules_filesystem_loader_la_CPPFLAGS = \
$(AM_CFLAGS) \
$(mod_cppflags) \
@EFL_CFLAGS@ \
@ELM_CFLAGS@ \
-I$(top_srcdir)/src/bin \
-I$(top_builddir)

src_modules_filesystem_loader_la_LIBADD = \
@EFL_LIBS@ \
@ELM_LIBS@

src_modules_filesystem_loader_la_LDFLAGS = -module -avoid-version

src_modules_filesystem_loader_la_LIBTOOLFLAGS = --tag=disable-static
endif

if MOD_ID3_LOADER
mod_LTLIBRARIES += src/modules/id3_loader.la

src_modules_id3_loader_la_SOURCES = \
src/modules/id3_loader.c \
src/bin/empc.h

src_modules_id3_loader_la_CPPFLAGS = \
$(AM_CFLAGS) \
$(mod_cppflags) \
@EFL_CFLAGS@ \
@ELM_CFLAGS@ \
@ID3_CFLAGS@ \
-I$(top_srcdir)/src/bin \
-I$(top_builddir)

src_modules_id3_loader_la_LIBADD = \
@EFL_LIBS@ \
@ELM_LIBS@ \
@ID3_LIBS@

src_modules_id3_loader_la_LDFLAGS = -module -avoid-version

src_modules_id3_loader_la_LIBTOOLFLAGS = --tag=disable-static
endif

if MOD_APL
mod_LTLIBRARIES += src/modules/auto_playlist_manager.la

src/modules/auto_playlist_manager.c: $(ELDBUS_H) $(AZY_H)

src_modules_auto_playlist_manager_la_SOURCES = \
src/modules/auto_playlist_manager.c \
src/bin/empc.h \
$(ELDBUS_H) \
$(AZY_H)

src_modules_auto_playlist_manager_la_CPPFLAGS = \
$(AM_CFLAGS) \
$(mod_cppflags) \
@EFL_CFLAGS@ \
@ELM_CFLAGS@ \
@EIO_CFLAGS@ \
-I$(top_srcdir)/src/bin \
-I$(top_builddir)/src/bin \
-I$(top_builddir)

src_modules_auto_playlist_manager_la_LIBADD = \
@EFL_LIBS@ \
@ELM_LIBS@ \
@EIO_LIBS@

src_modules_auto_playlist_manager_la_LDFLAGS = -module -avoid-version

src_modules_auto_playlist_manager_la_LIBTOOLFLAGS = --tag=disable-static
endif
