#include <e.h>
#include "e_mod_main.h"

struct _E_Config_Dialog_Data
{
   char *hostname;
   char *port;
   int   show_popup;

   int   show_osd;
   int osd_orient;
};

/* Protos */
static void        *_create_data(E_Config_Dialog *cfd);
static void         _free_data(E_Config_Dialog *cfd, E_Config_Dialog_Data *cfdata);
static Evas_Object *_basic_create_widgets(E_Config_Dialog *cfd, Evas *evas,
                                          E_Config_Dialog_Data *cfdata);
static int          _basic_apply_data(E_Config_Dialog *cfd,
                                      E_Config_Dialog_Data *cfdata);

void
_config_mpdule_module(Config_Item *ci)
{
   E_Config_Dialog *cfd;
   E_Config_Dialog_View *v;
   char buf[4096];

   v = E_NEW(E_Config_Dialog_View, 1);

   v->create_cfdata = _create_data;
   v->free_cfdata = _free_data;
   v->basic.apply_cfdata = _basic_apply_data;
   v->basic.create_widgets = _basic_create_widgets;

   snprintf(buf, sizeof (buf), "%s/e-module-empdule.edj",
            e_module_dir_get(mpdule_config->module));
   cfd =
     e_config_dialog_new(NULL, D_("eMPDule Configuration"), "eMPDule",
                         "_e_modules_mpdule_config_dialog", buf, 0, v, ci);
   mpdule_config->config_dialog = cfd;
}

static void
_fill_data(Config_Item *ci, E_Config_Dialog_Data *cfdata)
{
   char buf[128];

   cfdata->hostname = ci->hostname ? strdup(ci->hostname) : NULL;
   snprintf(buf, sizeof (buf), "%d", ci->port);
   cfdata->port = strdup(buf);
   cfdata->show_popup = ci->show_popup;
   cfdata->show_osd = mpdule_config->show_osd;
   cfdata->osd_orient = mpdule_config->osd_orient;
}

static void *
_create_data(E_Config_Dialog *cfd)
{
   E_Config_Dialog_Data *cfdata;
   Config_Item *ci;

   ci = cfd->data;
   cfdata = E_NEW(E_Config_Dialog_Data, 1);

   _fill_data(ci, cfdata);
   return cfdata;
}

static void
_free_data(E_Config_Dialog *cfd EINA_UNUSED, E_Config_Dialog_Data *cfdata)
 {
   if (!mpdule_config)
     return;
   mpdule_config->config_dialog = NULL;
   free(cfdata->hostname);
   E_FREE(cfdata);
}

static Evas_Object *
_basic_create_widgets(E_Config_Dialog *cfd EINA_UNUSED, Evas *evas, E_Config_Dialog_Data *cfdata)
{
   Evas_Object *o, *ol, *ow, *of, *ob, *hostname_entry, *port_entry;
   E_Radio_Group *rg;

   o = e_widget_list_add(evas, 0, 0);

   of = e_widget_frametable_add(evas, D_("Global Configuration"), 0);

   ob = e_widget_check_add(evas, D_("Show OSD"), &(cfdata->show_osd));
   e_widget_frametable_object_append(of, ob, 0, 0, 1, 1, 1, 0, 1, 0);

   ol = e_widget_table_add(evas, 1);
   e_widget_frametable_object_append(of, ol, 0, 1, 1, 1, 1, 0, 1, 0);
   rg = e_widget_radio_group_new(&(cfdata->osd_orient));
   ow = e_widget_radio_icon_add(evas, NULL, "preferences-position-bottom", //FIXME center
                                24, 24, E_GADCON_ORIENT_FLOAT, rg);
   e_widget_check_widget_disable_on_unchecked_add(ob, ow);
   e_widget_table_object_append(ol, ow, 1, 1, 1, 1, 1, 1, 1, 1);
   ow = e_widget_radio_icon_add(evas, NULL, "preferences-position-left",
                                24, 24, E_GADCON_ORIENT_LEFT, rg);
   e_widget_check_widget_disable_on_unchecked_add(ob, ow);
   e_widget_table_object_append(ol, ow, 0, 1, 1, 1, 1, 1, 1, 1);
   ow = e_widget_radio_icon_add(evas, NULL, "preferences-position-right",
                                24, 24, E_GADCON_ORIENT_RIGHT, rg);
   e_widget_check_widget_disable_on_unchecked_add(ob, ow);
   e_widget_table_object_append(ol, ow, 2, 1, 1, 1, 1, 1, 1, 1);
   ow = e_widget_radio_icon_add(evas, NULL, "preferences-position-top",
                                24, 24, E_GADCON_ORIENT_TOP, rg);
   e_widget_check_widget_disable_on_unchecked_add(ob, ow);
   e_widget_table_object_append(ol, ow, 1, 0, 1, 1, 1, 1, 1, 1);
   ow = e_widget_radio_icon_add(evas, NULL, "preferences-position-bottom",
                                24, 24, E_GADCON_ORIENT_BOTTOM, rg);
   e_widget_check_widget_disable_on_unchecked_add(ob, ow);
   e_widget_table_object_append(ol, ow, 1, 2, 1, 1, 1, 1, 1, 1);
   ow = e_widget_radio_icon_add(evas, NULL, "preferences-position-top-left",
                                24, 24, E_GADCON_ORIENT_CORNER_TL, rg);
   e_widget_check_widget_disable_on_unchecked_add(ob, ow);
   e_widget_table_object_append(ol, ow, 0, 0, 1, 1, 1, 1, 1, 1);
   ow = e_widget_radio_icon_add(evas, NULL, "preferences-position-top-right",
                                24, 24, E_GADCON_ORIENT_CORNER_TR, rg);
   e_widget_check_widget_disable_on_unchecked_add(ob, ow);
   e_widget_table_object_append(ol, ow, 2, 0, 1, 1, 1, 1, 1, 1);
   ow = e_widget_radio_icon_add(evas, NULL, "preferences-position-bottom-left",
                                24, 24, E_GADCON_ORIENT_CORNER_BL, rg);
   e_widget_check_widget_disable_on_unchecked_add(ob, ow);
   e_widget_table_object_append(ol, ow, 0, 2, 1, 1, 1, 1, 1, 1);
   ow = e_widget_radio_icon_add(evas, NULL, "preferences-position-bottom-right",
                                24, 24, E_GADCON_ORIENT_CORNER_BR, rg);
   e_widget_check_widget_disable_on_unchecked_add(ob, ow);
   e_widget_table_object_append(ol, ow, 2, 2, 1, 1, 1, 1, 1, 1);

   e_widget_list_object_append(o, of, 1, 1, 0.5);


   of = e_widget_frametable_add(evas, D_("Gadget Configuration"), 1);
   ob = e_widget_label_add(evas, D_("Hostname:"));
   e_widget_frametable_object_append(of, ob, 0, 0, 1, 1, 1, 0, 1, 0);
   hostname_entry =
     e_widget_entry_add(evas, &cfdata->hostname, NULL, NULL, NULL);
   e_widget_size_min_set(hostname_entry, 150, 20);
   e_widget_frametable_object_append(of, hostname_entry, 0, 1, 1, 1, 1, 0, 1,
                                     0);
   ob = e_widget_label_add(evas, D_("Port:"));
   e_widget_frametable_object_append(of, ob, 0, 2, 1, 1, 1, 0, 1, 0);
   port_entry = e_widget_entry_add(evas, &cfdata->port, NULL, NULL, NULL);
   e_widget_size_min_set(hostname_entry, 150, 20);
   e_widget_frametable_object_append(of, port_entry, 0, 3, 1, 1, 1, 0, 1, 0);
   ob = e_widget_check_add(evas, D_("Show Gadget Popup"), &(cfdata->show_popup));
   e_widget_frametable_object_append(of, ob, 0, 4, 1, 1, 1, 0, 1, 0);

   e_widget_list_object_append(o, of, 1, 1, 0.5);

   return o;
}

static int
_basic_apply_data(E_Config_Dialog *cfd, E_Config_Dialog_Data *cfdata)
{
   Config_Item *ci;

   ci = cfd->data;
   eina_stringshare_del(ci->hostname);
   ci->hostname = eina_stringshare_add(cfdata->hostname);
   ci->port = atoi(cfdata->port);
   ci->show_popup = cfdata->show_popup;
   mpdule_config->show_osd = !!cfdata->show_osd;
   mpdule_config->osd_orient = cfdata->osd_orient;
   e_config_save_queue();
   _mpdule_config_updated(ci);
   return 1;
}

