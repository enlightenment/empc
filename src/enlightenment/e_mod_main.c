#include <e.h>
#include "e_mod_main.h"
#undef E_FREE
#undef E_FREE_FUNC
#undef E_LIST_HANDLER_APPEND
#undef E_LIST_FOREACH
#include "empdd.h"
#include "eet_hash.h"
#include "eldbus_empd_empdd.h"
#include "eldbus_empd_empc.h"

static E_Config_DD *conf_edd = NULL;
static E_Config_DD *conf_item_edd = NULL;

Config *mpdule_config = NULL;

enum
{
   MPD_STATE_UNKNOWN = 0,
   MPD_STATE_STOP = 1,
   MPD_STATE_PLAY = 2,
   MPD_STATE_PAUSE = 3,
};

EAPI E_Module_Api e_modapi =
{
   E_MODULE_API_VERSION,
   "eMPDule"
};

/* Func Proto Requirements for Gadcon */
static E_Gadcon_Client *_gc_init(E_Gadcon *gc, const char *name,
                                 const char *id, const char *style);
static void             _gc_shutdown(E_Gadcon_Client *gcc);
static void             _gc_orient(E_Gadcon_Client *gcc, E_Gadcon_Orient orient);
static const char      *_gc_label(const E_Gadcon_Client_Class *client_class);
static Evas_Object     *_gc_icon(const E_Gadcon_Client_Class *client_class, Evas *evas);
static const char      *_gc_id_new(const E_Gadcon_Client_Class *client_class);



/* Define the class and gadcon functions this module provides */
static const E_Gadcon_Client_Class _gc_class = {
   GADCON_CLIENT_CLASS_VERSION,
   "empdule", {_gc_init, _gc_shutdown, _gc_orient, _gc_label, _gc_icon,
              _gc_id_new, NULL, e_gadcon_site_is_not_toolbar},
   E_GADCON_CLIENT_STYLE_PLAIN
};

typedef struct _Instance Instance;
struct _Instance
{
   E_Gadcon_Client *gcc;
   Evas_Object     *empdule;
   Evas_Object     *o_popup;
   Config_Item     *ci;
   E_Gadcon_Popup  *popup;
   Eina_Bool popup_creation_in_progress;
};

static Eina_List *handlers = NULL;
static Eldbus_Connection *empd_conn = NULL;
static Eldbus_Proxy *empd_proxy = NULL;
static Eldbus_Proxy *empc_proxy = NULL;

static Eina_Bool empd_connected = EINA_FALSE;
static Eina_Stringshare *empd_uri;
static unsigned int empd_duration;
static Eina_Stringshare *empd_artist;
static Eina_Stringshare *empd_composer;
static Eina_Stringshare *empd_title;
static Eina_Stringshare *empd_album;
static int empd_track;
static Eina_Stringshare *empd_name;
static Eina_Stringshare *empd_date;
static Eina_Stringshare *empd_disc;
static Eina_Stringshare *empd_genre;
static int empd_song_pos;
static int empd_state = -1;

static Eina_Stringshare *empc_bg_file;
static Eina_Stringshare *empc_bg_key;

static void      _mpdule_connect(Instance *inst);
static void      _mpdule_popup_destroy(Instance *inst);

static void
_mpdule_status_update(Instance *inst)
{
   const char *status = NULL;

   switch (empd_state)
     {
      case MPD_STATE_UNKNOWN:
        status = D_("Unknown");
        break;
      case MPD_STATE_STOP:
        status = D_("Stopped");
        break;
      case MPD_STATE_PLAY:
        status = D_("Playing");
        break;
      case MPD_STATE_PAUSE:
        status = D_("Paused");
        break;
     }
    
   edje_object_part_text_set(inst->empdule, "empdule.status", status);
   edje_object_part_text_set(inst->o_popup, "empdule.status", status);
}

static void
_mpdule_info_update(Instance *inst)
{
   char buf[128];

   snprintf(buf, sizeof(buf), "%d", empd_track);

#define FILL_IN(_var)                                   \
   edje_object_part_text_set(inst->empdule, "empdule."#_var, empd_##_var ?: ""); \
   edje_object_part_text_set(inst->o_popup, "empdule."#_var, empd_##_var ?: "")

   FILL_IN(artist);
   FILL_IN(title);
   FILL_IN(album);
   FILL_IN(date);
   FILL_IN(genre);
   FILL_IN(composer);
   FILL_IN(uri);

   edje_object_part_text_set(inst->empdule, "empdule.track", buf);
   edje_object_part_text_set(inst->o_popup, "empdule.track", buf);
#undef FILL_IN
}

static void
_mpdule_cover_update(Instance *inst)
{
   Evas_Object *ic, *lt;
   int w, h;

   lt = edje_object_part_swallow_get(inst->empdule, "empdule.swallow.album");
   ic = e_livethumb_thumb_get(lt);
   if (!empc_bg_file)
     {
        e_livethumb_thumb_set(lt, NULL);
        evas_object_del(ic);
        return;
     }
   if (!ic)
     ic = e_icon_add(e_livethumb_evas_get(lt));
   e_icon_file_key_set(ic, empc_bg_file, empc_bg_key);
   e_livethumb_thumb_set(lt, ic);
   e_icon_size_get(ic, &w, &h);
   evas_object_size_hint_aspect_set(lt, EVAS_ASPECT_CONTROL_BOTH, w, h);
   if (w > h)
     {
        h = 192 * h / w;
        w = 192;
     }
   else if (h > w)
     {
        w = 192 * w / h;
        h = 192;
     }
   else
     w = h = 192;
   e_livethumb_vsize_set(lt, w, h);
}

static void
_mpdule_osd_hidden()
{
   E_FREE_FUNC(mpdule_config->osd, evas_object_del);
}

static void
_mpdule_osd_show(void)
{
   Evas_Object *o, *ic;
   char buf[PATH_MAX];
   E_Comp *comp;
   E_Zone *zone;
   int x, y, w, h;

   if (!mpdule_config->show_osd) return;
   if (!empc_bg_file) return;

#if E_VERSION_MAJOR >= 20
   comp = e_comp;
   zone = e_zone_current_get();
#else
   comp = e_comp_get(NULL);
   zone = e_zone_current_get(comp);
#endif
   mpdule_config->osd = o = edje_object_add(comp->evas);
   evas_object_layer_set(o, E_LAYER_POPUP);
   evas_object_show(o);
   evas_object_pass_events_set(o, 1);
   edje_object_signal_callback_add(o, "empdule,state,hidden", "empdule", _mpdule_osd_hidden, NULL);
   snprintf(buf, sizeof(buf), "%s/empdule.edj",
            e_module_dir_get(mpdule_config->module));
   if (!e_theme_edje_object_set(o, NULL, "e/modules/empdule/osd"))
     edje_object_file_set(o, buf, "e/modules/empdule/osd");
   ic = e_icon_add(evas_object_evas_get(o));
   e_comp_object_util_del_list_append(o, ic);
   e_icon_file_key_set(ic, empc_bg_file, empc_bg_key);
   e_icon_size_get(ic, &w, &h);
   evas_object_size_hint_max_set(ic, w * 1.5, h * 1.5);
   evas_object_size_hint_aspect_set(ic, EVAS_ASPECT_CONTROL_BOTH, w, h);
   edje_object_part_swallow(o, "empdule.swallow.album", ic);
   edje_object_signal_emit(o, "empdule,action,show", "empdule");
   edje_object_size_min_restricted_calc(o, &w, &h, zone->w, zone->h);
   evas_object_resize(o, w, h);
   e_comp_object_util_center_pos_get(o, &x, &y);
   switch (mpdule_config->osd_orient)
     {
      case E_GADCON_ORIENT_FLOAT:
        e_comp_object_util_center(o);
        break;
      case E_GADCON_ORIENT_LEFT:
        evas_object_move(o, zone->x, y);
        break;
      case E_GADCON_ORIENT_RIGHT:
        evas_object_move(o, zone->x + (zone->w - w), y);
        break;
      case E_GADCON_ORIENT_TOP:
        evas_object_move(o, x, zone->y);
        break;
      case E_GADCON_ORIENT_BOTTOM:
        evas_object_move(o, x, zone->y + (zone->h - h));
        break;
      case E_GADCON_ORIENT_CORNER_TL:
        evas_object_move(o, zone->x, zone->y);
        break;
      case E_GADCON_ORIENT_CORNER_TR:
        evas_object_move(o, zone->x + (zone->w - w), zone->y);
        break;
      case E_GADCON_ORIENT_CORNER_BL:
        evas_object_move(o, zone->x, zone->y + (zone->h - h));
        break;
      case E_GADCON_ORIENT_CORNER_BR:
        evas_object_move(o, zone->x + (zone->w - w), zone->y + (zone->h - h));
        break;
     }
}

static void
_mpdule_cb_play()
{
   empd_empdd_play_call(empd_proxy);
}

static void
_mpdule_cb_previous()
{
   empd_empdd_previous_call(empd_proxy);
}

static void
_mpdule_cb_next()
{
   empd_empdd_next_call(empd_proxy);
}

static void
_mpdule_cb_stop()
{
   empd_empdd_next_call(empd_proxy);
}

static void
_mpdule_cb_pause()
{
   empd_empdd_toggle_pause_call(empd_proxy);
}

static Config_Item *
_mpdule_config_item_get(const char *id)
{
   Config_Item *ci;

   GADCON_CLIENT_CONFIG_GET(Config_Item, mpdule_config->items, _gc_class, id);

   ci = E_NEW(Config_Item, 1);
   ci->id = eina_stringshare_add(id);
   ci->port = -1;
   ci->show_popup = 1;

   mpdule_config->items = eina_list_append(mpdule_config->items, ci);
   return ci;
}

static void
_mpdule_connect(Instance *inst)
{
   empd_empdd_connect_call(empd_proxy, inst->ci->hostname ?: "", inst->ci->port, ""); //FIXME password
}

static void
_mpdule_popup_create(Instance *inst)
{
   if (inst->popup)
     return;

   if (inst->ci->show_popup)
     {
        Evas *evas;
        Evas_Object *o_popup;

        if (inst->popup_creation_in_progress)
            return;
        inst->popup_creation_in_progress = EINA_TRUE;

        inst->popup = e_gadcon_popup_new(inst->gcc, EINA_FALSE);
#if E_VERSION_MAJOR >= 20
        evas = e_comp->evas;
#else
        evas = e_comp_get(NULL)->evas;
#endif
        o_popup = edje_object_add(evas);
        if (!e_theme_edje_object_set
              (o_popup, "base/theme/modules/empdule", "e/modules/empdule/popup"))
          {
             char buf[4096];

             snprintf(buf, sizeof(buf), "%s/empdule.edj",
                      e_module_dir_get(mpdule_config->module));
             edje_object_file_set(o_popup, buf, "e/modules/empdule/popup");
          }
        edje_object_part_text_set(o_popup, "empdule.artist_label", D_("Artist:"));
        edje_object_part_text_set(o_popup, "empdule.title_label", D_("Title:"));
        edje_object_part_text_set(o_popup, "empdule.album_label", D_("Album:"));
        edje_object_part_text_set(o_popup, "empdule.genre_label", D_("Genre:"));
        e_gadcon_popup_content_set(inst->popup, o_popup);
        e_gadcon_popup_show(inst->popup);
        edje_object_size_min_calc(o_popup, NULL, NULL);
        inst->o_popup = o_popup;
        edje_object_signal_callback_add(o_popup, "empdule,play", "",
                                        _mpdule_cb_play, inst);
        edje_object_signal_callback_add(o_popup, "empdule,stop", "",
                                        _mpdule_cb_stop, inst);
        edje_object_signal_callback_add(o_popup, "empdule,pause", "",
                                        _mpdule_cb_pause, inst);
        edje_object_signal_callback_add(o_popup, "empdule,next", "",
                                        _mpdule_cb_next, inst);
        edje_object_signal_callback_add(o_popup, "empdule,previous", "",
                                        _mpdule_cb_previous, inst);

#define DISPLAY_INFO(_var) \
             edje_object_part_text_set(o_popup, "empdule."#_var, empd_##_var ?: "");
        DISPLAY_INFO(artist);
        DISPLAY_INFO(title);
        DISPLAY_INFO(album);
        DISPLAY_INFO(date);
        DISPLAY_INFO(genre);
        DISPLAY_INFO(composer);
        //DISPLAY_INFO(time);
        DISPLAY_INFO(uri);
#undef DISPLAY_INFO

        {
           char buf[128];

           snprintf(buf, sizeof(buf), "%d", empd_track);
           edje_object_part_text_set(o_popup, "empdule.track", buf);
        }

        inst->popup_creation_in_progress = EINA_FALSE;
     }
}

static void
_mpdule_menu_cb_post()
{
   if (!mpdule_config->menu)
     return;
   e_object_del(E_OBJECT(mpdule_config->menu));
   mpdule_config->menu = NULL;
}

static void
_mpdule_menu_cb_configure(void *data, E_Menu *m EINA_UNUSED, E_Menu_Item *mi EINA_UNUSED)
{
   Instance *inst = data;
   _config_mpdule_module(inst->ci);
}

static void
_mpdule_cb_mouse_down(void *data, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info)
{
   Evas_Event_Mouse_Down *ev = event_info;
   Instance *inst = data;

   if ((ev->button == 3) && (!mpdule_config->menu))
     {
        E_Menu *m;
        E_Menu_Item *mi;
        int x, y, w, h;

        m = e_menu_new();
        mi = e_menu_item_new(m);
        e_menu_item_label_set(mi, D_("Settings"));
        e_util_menu_item_theme_icon_set(mi, "preferences-system");
        e_menu_item_callback_set(mi, _mpdule_menu_cb_configure, inst);

        m = e_gadcon_client_util_menu_items_append(inst->gcc, m, 0);
        e_menu_post_deactivate_callback_set(m, _mpdule_menu_cb_post, inst);
        mpdule_config->menu = m;

        e_gadcon_canvas_zone_geometry_get(inst->gcc->gadcon, &x, &y, &w, &h);
        e_menu_activate_mouse(m,
#if E_VERSION_MAJOR >= 20
                              e_zone_current_get(),
#else
                              e_util_zone_current_get(e_manager_current_get()),
#endif
                              x + ev->output.x,
                              y + ev->output.y, 1, 1,
                              E_MENU_POP_DIRECTION_DOWN, ev->timestamp);
        evas_event_feed_mouse_up(inst->gcc->gadcon->evas, ev->button,
                                 EVAS_BUTTON_NONE, ev->timestamp, NULL);
     }
   else if (ev->button == 1)
     {
        if (inst->popup) _mpdule_popup_destroy(inst);
        else _mpdule_popup_create(inst);
     }
}

static void
_mpdule_cb_mouse_in(void *data, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   Instance *inst = data;

   if ((!inst) || (!inst->ci->show_popup))
     return;
   if (!inst->popup)
     {
        _mpdule_popup_create(inst);
     }
}

static void
_mpdule_cb_mouse_out(void *data, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   Instance *inst = data;

   if ((!inst) || ((!inst->ci->show_popup) && (!inst->popup)))
     return;
   if (inst->popup)
     {
        _mpdule_popup_destroy(inst);
     }
}

void
_mpdule_config_updated(Config_Item *ci)
{
   Eina_List *l;
   Instance *inst;

   if (!mpdule_config)
     return;

   EINA_LIST_FOREACH(mpdule_config->instances, l, inst)
     {
        if (inst->ci != ci) continue;

        _mpdule_connect(inst);
        _mpdule_popup_destroy(inst);
        _mpdule_status_update(inst);
        break;
     }
}

static void
empd_isconnected_cb(Eldbus_Proxy *proxy EINA_UNUSED, void *data EINA_UNUSED, Eldbus_Pending *pending EINA_UNUSED, Eldbus_Error_Info *error EINA_UNUSED, Eina_Bool connected, const char *host EINA_UNUSED, unsigned int port EINA_UNUSED)
{
   empd_connected = connected;
   if (connected)
     empd_empdd_current_song_call(empd_proxy);
   else
     _mpdule_connect(eina_list_data_get(mpdule_config->instances));
}

static void
empd_getbg_cb(Eldbus_Proxy *proxy EINA_UNUSED, void *data EINA_UNUSED, Eldbus_Pending *pending EINA_UNUSED, Eldbus_Error_Info *error EINA_UNUSED, const char *file, const char *key)
{
   if ((error) || (error->error[0]))
     {
        char buf[4096];
        char *a = NULL, *b = NULL;
        int num = 0;
        Eet_File *ef = NULL;

        eina_stringshare_replace(&empc_bg_file, NULL);
        eina_stringshare_replace(&empc_bg_key, NULL);

        if ((!empd_album) || (!empd_artist)) goto end;

        snprintf(buf, sizeof(buf), "%s/empc/metadata/images.eet", efreet_cache_home_get());
        ef = eet_open(buf, EET_FILE_MODE_READ);
        if (!ef) goto end;
        a = strdupa(empd_artist);
        eina_str_tolower(&a);
        b = strdupa(empd_album);
        eina_str_tolower(&b);
        snprintf(buf, sizeof(buf), "%s:::%s", a ?: "", b ?: "");
        free(eet_list(ef, buf, &num));
        if (num)
          {
             eina_stringshare_replace(&empc_bg_file, eet_file_get(ef));
             eina_stringshare_replace(&empc_bg_key, buf);
          }
        else
          {
             Eina_Stringshare *url;

             url = eet_hash_get(ef, buf);
             if (url && (url[0] == '/')) //only get local files
               {
                  empc_bg_file = url;
                  url = NULL;
               }
             eina_stringshare_del(url);
          }
        eet_close(ef);
     }
   else
     {
        eina_stringshare_replace(&empc_bg_file, file);
        eina_stringshare_replace(&empc_bg_key, key);
     }
end:
   _mpdule_osd_show();
   E_LIST_FOREACH(mpdule_config->instances, _mpdule_cover_update);
}

static Eina_Bool
empd_status(void *d EINA_UNUSED, int t EINA_UNUSED, Empd_Empdd_Status_Data *ev)
{
   if ((empd_state != -1) && ((unsigned int)empd_state == ev->state)) return ECORE_CALLBACK_RENEW;

   empd_state = ev->state;
   E_LIST_FOREACH(mpdule_config->instances, _mpdule_status_update);
   return ECORE_CALLBACK_RENEW;
}

static Eina_Bool
empc_bg_changed(void *d EINA_UNUSED, int t EINA_UNUSED, Empd_Empc_BackgroundChanged_Data *ev)
{
   if ((!e_util_strcmp(ev->file, empc_bg_file)) && (!e_util_strcmp(ev->key, empc_bg_key)))
     return ECORE_CALLBACK_RENEW;
   eina_stringshare_replace(&empc_bg_file, ev->file);
   eina_stringshare_replace(&empc_bg_key, ev->key);
   _mpdule_osd_show();
   E_LIST_FOREACH(mpdule_config->instances, _mpdule_cover_update);
   return ECORE_CALLBACK_RENEW;
}

static Eina_Bool
empd_current_song(void *d EINA_UNUSED, int t EINA_UNUSED, Empd_Empdd_CurrentSong_Data *ev)
{
   if (((!empc_bg_file) || (!empc_bg_file[0])) ||
       (e_util_strcmp(empd_artist, ev->artist) || e_util_strcmp(empd_album, ev->album)))
     empd_empc_get_background_call(empd_proxy, empd_getbg_cb, NULL);

#define COPY(X) \
   eina_stringshare_replace(&empd_##X, ev->X)

   COPY(artist);
   COPY(title);
   COPY(album);
   COPY(date);
   COPY(genre);
   COPY(composer);
   COPY(uri);

   empd_duration = ev->duration;
   empd_track = ev->track;
   empd_song_pos = ev->song_pos;

   E_LIST_FOREACH(mpdule_config->instances, _mpdule_info_update);

   return ECORE_CALLBACK_RENEW;
}

static Eina_Bool
empd_connected_event(void *d EINA_UNUSED, int t EINA_UNUSED, Empd_Empdd_Connected_Data *ev EINA_UNUSED)
{
   empd_connected = EINA_TRUE;
   return ECORE_CALLBACK_RENEW;
}

static Eina_Bool
empd_disconnected_event(void *d EINA_UNUSED, int t EINA_UNUSED, Empd_Empdd_Disconnected_Data *ev EINA_UNUSED)
{
   empd_connected = EINA_FALSE;
   return ECORE_CALLBACK_RENEW;
}

static E_Config_Dialog *
_mpdule_config_dialog()
{
   if (!mpdule_config) return NULL;
   if (mpdule_config->config_dialog) return NULL;
   _config_mpdule_module(eina_list_data_get(mpdule_config->items));
   return mpdule_config->config_dialog;
}

EAPI void *
e_modapi_init(E_Module *m)
{
   char buf[4096];

   snprintf(buf, sizeof(buf), "%s/locale", e_module_dir_get(m));
   bindtextdomain(PACKAGE, buf);
   bind_textdomain_codeset(PACKAGE, "UTF-8");

   conf_item_edd = E_CONFIG_DD_NEW("eMPDule_Config_Item", Config_Item);
#undef T
#undef D
#define T Config_Item
#define D conf_item_edd
   E_CONFIG_VAL(D, T, id, STR);
   E_CONFIG_VAL(D, T, hostname, STR);
   E_CONFIG_VAL(D, T, port, INT);
   E_CONFIG_VAL(D, T, show_popup, UCHAR);

   conf_edd = E_CONFIG_DD_NEW("eMPDule_Config", Config);
#undef T
#undef D
#define T Config
#define D conf_edd
   E_CONFIG_VAL(D, T, show_osd, UCHAR);
   E_CONFIG_LIST(D, T, items, conf_item_edd);

   mpdule_config = e_config_domain_load("module.empdule", conf_edd);
   if (!mpdule_config)
     {
        Config_Item *ci;

        mpdule_config = E_NEW(Config, 1);
        mpdule_config->show_osd = 1;

        ci = E_NEW(Config_Item, 1);
        ci->id = eina_stringshare_add("0");
        ci->port = -1;
        ci->show_popup = 1;

        mpdule_config->items = eina_list_append(mpdule_config->items, ci);
     }
   mpdule_config->module = m;
   snprintf(buf, sizeof(buf), "%s/e-module-empdule.edj", e_module_dir_get(m));
   e_configure_registry_item_add("extensions/empdule", 40, D_("eMPDule"), buf,
                                 NULL, _mpdule_config_dialog);

   e_gadcon_provider_register(&_gc_class);
   eet_hash_init();
   empd_conn = eldbus_connection_get(ELDBUS_CONNECTION_TYPE_SESSION);
   empd_proxy = empd_empdd_proxy_get(empd_conn, EMPDD_METHOD_BASE, NULL);
   empc_proxy = empd_empc_proxy_get(empd_conn, "org.empd.empc", NULL);
   empd_empdd_is_connected_call(empd_proxy, empd_isconnected_cb, NULL);
   E_LIST_HANDLER_APPEND(handlers, EMPD_EMPDD_CONNECTED_EVENT, empd_connected_event, NULL);
   E_LIST_HANDLER_APPEND(handlers, EMPD_EMPDD_DISCONNECTED_EVENT, empd_disconnected_event, NULL);
   E_LIST_HANDLER_APPEND(handlers, EMPD_EMPDD_STATUS_EVENT, empd_status, NULL);
   E_LIST_HANDLER_APPEND(handlers, EMPD_EMPDD_CURRENT_SONG_EVENT, empd_current_song, NULL);
   E_LIST_HANDLER_APPEND(handlers, EMPD_EMPC_BACKGROUND_CHANGED_EVENT, empc_bg_changed, NULL);
   return m;
}

EAPI int
e_modapi_shutdown(E_Module *m EINA_UNUSED)
{
   Config_Item *ci;

   mpdule_config->module = NULL;
   e_gadcon_provider_unregister(&_gc_class);

   e_configure_registry_item_del("extensions/empdule");

   E_FREE_FUNC(mpdule_config->config_dialog, e_object_del);
   if (mpdule_config->menu)
     {
        e_menu_post_deactivate_callback_set(mpdule_config->menu, NULL, NULL);
        E_FREE_FUNC(mpdule_config->menu, e_object_del);
     }

   EINA_LIST_FREE(mpdule_config->items, ci)
     {
        eina_stringshare_del(ci->id);
        free(ci);
     }
   empd_empdd_proxy_unref(empd_proxy);
   empd_empc_proxy_unref(empc_proxy);
   eldbus_connection_unref(empd_conn);
   E_FREE_LIST(handlers, ecore_event_handler_del);
   E_FREE_FUNC(mpdule_config->osd, evas_object_del);

   E_FREE(mpdule_config);
   E_CONFIG_DD_FREE(conf_item_edd);
   E_CONFIG_DD_FREE(conf_edd);
   eet_hash_shutdown();

   eina_stringshare_replace(&empd_uri, NULL);
   eina_stringshare_replace(&empd_artist, NULL);
   eina_stringshare_replace(&empd_title, NULL);
   eina_stringshare_replace(&empd_album, NULL);
   eina_stringshare_replace(&empd_name, NULL);
   eina_stringshare_replace(&empd_date, NULL);
   eina_stringshare_replace(&empd_disc, NULL);
   empd_state = -1;
   return 1;
}

EAPI int
e_modapi_save(E_Module *m EINA_UNUSED)
{
   e_config_domain_save("module.empdule", conf_edd, mpdule_config);
   return 1;
}

static void
_mpdule_popup_destroy(Instance *inst)
{
   if (inst->popup)
     {
        E_Gadcon_Popup *popup = inst->popup;

        inst->popup = NULL;
        inst->o_popup = NULL;
        e_object_del(E_OBJECT(popup));
     }
}


static E_Gadcon_Client *
_gc_init(E_Gadcon *gc, const char *name, const char *id, const char *style)
{
   Evas_Object *o, *lt;
   E_Gadcon_Client *gcc;
   Instance *inst;
   char buf[4096];

   inst = E_NEW(Instance, 1);

   inst->ci = _mpdule_config_item_get(id);
   if (!inst->ci->id)
     inst->ci->id = eina_stringshare_add(id);

   o = edje_object_add(gc->evas);
   snprintf(buf, sizeof(buf), "%s/empdule.edj",
            e_module_dir_get(mpdule_config->module));
   if (!e_theme_edje_object_set
         (o, "base/theme/modules/empdule", "e/modules/empdule/main"))
     edje_object_file_set(o, buf, "e/modules/empdule/main");
   evas_object_show(o);

   gcc = e_gadcon_client_new(gc, name, id, style, o);
   gcc->data = inst;
   inst->gcc = gcc;
   inst->empdule = o;

   lt = e_livethumb_add(gc->evas);
   evas_object_show(lt);
   edje_object_part_swallow(o, "empdule.swallow.album", lt);

   evas_object_event_callback_add(o, EVAS_CALLBACK_MOUSE_DOWN,
                                  _mpdule_cb_mouse_down, inst);
   evas_object_event_callback_add(inst->empdule, EVAS_CALLBACK_MOUSE_IN,
                                  _mpdule_cb_mouse_in, inst);
   evas_object_event_callback_add(inst->empdule, EVAS_CALLBACK_MOUSE_OUT,
                                  _mpdule_cb_mouse_out, inst);
   edje_object_signal_callback_add(o, "empdule,play", "", _mpdule_cb_play,
                                   inst);
   edje_object_signal_callback_add(o, "empdule,stop", "", _mpdule_cb_stop,
                                   inst);
   edje_object_signal_callback_add(o, "empdule,pause", "", _mpdule_cb_pause,
                                   inst);
   edje_object_signal_callback_add(o, "empdule,next", "", _mpdule_cb_next,
                                   inst);
   edje_object_signal_callback_add(o, "empdule,previous", "",
                                   _mpdule_cb_previous, inst);
   //_mpdule_connect(inst);
   if (empd_connected)
     _mpdule_status_update(inst);

   mpdule_config->instances =
     eina_list_append(mpdule_config->instances, inst);
   return gcc;
}

static void
_gc_shutdown(E_Gadcon_Client *gcc)
{
   Instance *inst;

   inst = gcc->data;
   mpdule_config->instances =
     eina_list_remove(mpdule_config->instances, inst);

   evas_object_event_callback_del(inst->empdule, EVAS_CALLBACK_MOUSE_DOWN,
                                  _mpdule_cb_mouse_down);
   evas_object_event_callback_del(inst->empdule, EVAS_CALLBACK_MOUSE_IN,
                                  _mpdule_cb_mouse_in);
   evas_object_event_callback_del(inst->empdule, EVAS_CALLBACK_MOUSE_OUT,
                                  _mpdule_cb_mouse_out);
   _mpdule_popup_destroy(inst);
   evas_object_del(inst->empdule);
   gcc->data = NULL;
}

static void
_gc_orient(E_Gadcon_Client *gcc, E_Gadcon_Orient orient EINA_UNUSED)
{
   Instance *inst;
   Evas_Coord mw, mh;

   inst = gcc->data;
   edje_object_size_min_calc(inst->empdule, &mw, &mh);
   e_gadcon_client_min_size_set(gcc, mw, mh);
}

static const char *
_gc_label(const E_Gadcon_Client_Class *client_class EINA_UNUSED)
{
   return D_("MPDule");
}

static Evas_Object *
_gc_icon(const E_Gadcon_Client_Class *client_class EINA_UNUSED, Evas *evas)
{
   Evas_Object *o;
   char buf[4096];

   o = edje_object_add(evas);
   snprintf(buf, sizeof(buf), "%s/e-module-empdule.edj",
            e_module_dir_get(mpdule_config->module));
   edje_object_file_set(o, buf, "icon");
   return o;
}

static const char *
_gc_id_new(const E_Gadcon_Client_Class *client_class EINA_UNUSED)
{
   Config_Item *ci = _mpdule_config_item_get(NULL);
   return ci->id;
}
