EXTRA_DIST += \
src/enlightenment/module.desktop.in \
src/enlightenment/e-module-empdule.edc \
src/enlightenment/empdule.edc \
$(wildcard src/enlightenment/images/*.png)


if MPDULE
pkgdir = $(e_module_dir)/empdule/$(E_MODULE_ARCH)
pkg_LTLIBRARIES = src/enlightenment/module.la

src_enlightenment_module_la_SOURCES = \
src/enlightenment/e_mod_main.c \
src/enlightenment/e_mod_main.h \
src/enlightenment/e_mod_config.c \
src/modules/eet_hash.c \
src/modules/eet_hash.h \
$(ELDBUS_SRC)

src_enlightenment_module_la_CPPFLAGS = \
-I$(top_builddir) \
-I$(top_builddir)/src/bin \
-I$(top_srcdir)/src/bin \
-I$(top_srcdir)/src/modules \
@E_CFLAGS@ \
@EFL_CFLAGS@
src_enlightenment_module_la_LIBADD = @E_LIBS@ @EFL_LIBS@
src_enlightenment_module_la_LDFLAGS = -module -avoid-version

filesdir = $(e_module_dir)/empdule
files_DATA = \
src/enlightenment/e-module-empdule.edj \
src/enlightenment/module.desktop \
src/enlightenment/empdule.edj

src/enlightenment/module.desktop: src/enlightenment/module.desktop.in

E_EDJE_FLAGS = -v \
	     -id $(top_srcdir)/src/enlightenment/images

src/enlightenment/%.edj:  src/enlightenment/%.edc
	@edje_cc@ $(E_EDJE_FLAGS) $< $@

endif
