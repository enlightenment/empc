#include "Empd_Common_Azy.h"

Eina_Value *Empd_Empdd_Song_to_azy_value(const Empd_Empdd_Song * azy_user_type)
{
  Eina_Value *value_struct = NULL;
  size_t offset = 0;
  Eina_Value_Struct_Member *members = NULL;
  Eina_Value *val = NULL;
  Eina_Value_Struct_Desc *st_desc;

  if (!azy_user_type) return NULL;

  st_desc = eina_value_util_struct_desc_new();
  members = malloc(12 * sizeof(Eina_Value_Struct_Member));
  st_desc->members = members;
  st_desc->member_count = 12;
  members[0].name = eina_stringshare_add("uri");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_STRINGSHARE, offset);
  members[0].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_STRINGSHARE);
  members[0].type = EINA_VALUE_TYPE_STRINGSHARE;
  members[1].name = eina_stringshare_add("last_modified");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_TIMESTAMP, offset);
  members[1].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_TIMESTAMP);
  members[1].type = EINA_VALUE_TYPE_TIMESTAMP;
  members[2].name = eina_stringshare_add("duration");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_TIMESTAMP, offset);
  members[2].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_TIMESTAMP);
  members[2].type = EINA_VALUE_TYPE_TIMESTAMP;
  members[3].name = eina_stringshare_add("artist");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_STRINGSHARE, offset);
  members[3].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_STRINGSHARE);
  members[3].type = EINA_VALUE_TYPE_STRINGSHARE;
  members[4].name = eina_stringshare_add("title");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_STRINGSHARE, offset);
  members[4].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_STRINGSHARE);
  members[4].type = EINA_VALUE_TYPE_STRINGSHARE;
  members[5].name = eina_stringshare_add("album");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_STRINGSHARE, offset);
  members[5].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_STRINGSHARE);
  members[5].type = EINA_VALUE_TYPE_STRINGSHARE;
  members[6].name = eina_stringshare_add("track");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_INT, offset);
  members[6].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_INT);
  members[6].type = EINA_VALUE_TYPE_INT;
  members[7].name = eina_stringshare_add("name");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_STRINGSHARE, offset);
  members[7].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_STRINGSHARE);
  members[7].type = EINA_VALUE_TYPE_STRINGSHARE;
  members[8].name = eina_stringshare_add("date");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_STRINGSHARE, offset);
  members[8].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_STRINGSHARE);
  members[8].type = EINA_VALUE_TYPE_STRINGSHARE;
  members[9].name = eina_stringshare_add("disc");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_STRINGSHARE, offset);
  members[9].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_STRINGSHARE);
  members[9].type = EINA_VALUE_TYPE_STRINGSHARE;
  members[10].name = eina_stringshare_add("song_pos");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_INT, offset);
  members[10].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_INT);
  members[10].type = EINA_VALUE_TYPE_INT;
  members[11].name = eina_stringshare_add("songid");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_INT, offset);
  members[11].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_INT);
  members[11].type = EINA_VALUE_TYPE_INT;
  st_desc->size = offset;
  value_struct = eina_value_struct_new(st_desc);

  val = eina_value_util_stringshare_new(azy_user_type->uri);
  eina_value_struct_value_set(value_struct, "uri", val);
  eina_value_free(val);
  val = eina_value_util_time_new(azy_user_type->last_modified);
  eina_value_struct_value_set(value_struct, "last_modified", val);
  eina_value_free(val);
  val = eina_value_util_time_new(azy_user_type->duration);
  eina_value_struct_value_set(value_struct, "duration", val);
  eina_value_free(val);
  val = eina_value_util_stringshare_new(azy_user_type->artist);
  eina_value_struct_value_set(value_struct, "artist", val);
  eina_value_free(val);
  val = eina_value_util_stringshare_new(azy_user_type->title);
  eina_value_struct_value_set(value_struct, "title", val);
  eina_value_free(val);
  val = eina_value_util_stringshare_new(azy_user_type->album);
  eina_value_struct_value_set(value_struct, "album", val);
  eina_value_free(val);
  val = eina_value_util_int_new(azy_user_type->track);
  eina_value_struct_value_set(value_struct, "track", val);
  eina_value_free(val);
  val = eina_value_util_stringshare_new(azy_user_type->name);
  eina_value_struct_value_set(value_struct, "name", val);
  eina_value_free(val);
  val = eina_value_util_stringshare_new(azy_user_type->date);
  eina_value_struct_value_set(value_struct, "date", val);
  eina_value_free(val);
  val = eina_value_util_stringshare_new(azy_user_type->disc);
  eina_value_struct_value_set(value_struct, "disc", val);
  eina_value_free(val);
  val = eina_value_util_int_new(azy_user_type->song_pos);
  eina_value_struct_value_set(value_struct, "song_pos", val);
  eina_value_free(val);
  val = eina_value_util_int_new(azy_user_type->songid);
  eina_value_struct_value_set(value_struct, "songid", val);
  eina_value_free(val);
  return value_struct;
}

Eina_Bool azy_value_to_Empd_Empdd_Song(const Eina_Value *value_struct, Empd_Empdd_Song * *azy_user_type)
{
  Empd_Empdd_Song * azy_user_type_tmp = NULL;
  Eina_Value val;
  unsigned int arg = 0;
  Eina_Bool found = EINA_FALSE;

  EINA_SAFETY_ON_NULL_RETURN_VAL(azy_user_type, EINA_FALSE);
  EINA_SAFETY_ON_NULL_RETURN_VAL(value_struct, EINA_FALSE);

  EINA_SAFETY_ON_TRUE_RETURN_VAL(eina_value_type_get(value_struct) != EINA_VALUE_TYPE_STRUCT, EINA_FALSE);

  azy_user_type_tmp = Empd_Empdd_Song_new();
  if ((!arg) && eina_value_struct_value_get(value_struct, "uri", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    if (arg)
      {
        char *str = NULL;

        if (eina_value_get(&val, &str))
          azy_user_type_tmp->uri = eina_stringshare_add(str);
      }
    else
      eina_value_util_stringshare_copy(&val, &azy_user_type_tmp->uri);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "last_modified", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    eina_value_get(&val, &azy_user_type_tmp->last_modified);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "duration", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    eina_value_get(&val, &azy_user_type_tmp->duration);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "artist", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    if (arg)
      {
        char *str = NULL;

        if (eina_value_get(&val, &str))
          azy_user_type_tmp->artist = eina_stringshare_add(str);
      }
    else
      eina_value_util_stringshare_copy(&val, &azy_user_type_tmp->artist);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "title", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    if (arg)
      {
        char *str = NULL;

        if (eina_value_get(&val, &str))
          azy_user_type_tmp->title = eina_stringshare_add(str);
      }
    else
      eina_value_util_stringshare_copy(&val, &azy_user_type_tmp->title);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "album", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    if (arg)
      {
        char *str = NULL;

        if (eina_value_get(&val, &str))
          azy_user_type_tmp->album = eina_stringshare_add(str);
      }
    else
      eina_value_util_stringshare_copy(&val, &azy_user_type_tmp->album);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "track", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    eina_value_get(&val, &azy_user_type_tmp->track);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "name", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    if (arg)
      {
        char *str = NULL;

        if (eina_value_get(&val, &str))
          azy_user_type_tmp->name = eina_stringshare_add(str);
      }
    else
      eina_value_util_stringshare_copy(&val, &azy_user_type_tmp->name);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "date", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    if (arg)
      {
        char *str = NULL;

        if (eina_value_get(&val, &str))
          azy_user_type_tmp->date = eina_stringshare_add(str);
      }
    else
      eina_value_util_stringshare_copy(&val, &azy_user_type_tmp->date);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "disc", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    if (arg)
      {
        char *str = NULL;

        if (eina_value_get(&val, &str))
          azy_user_type_tmp->disc = eina_stringshare_add(str);
      }
    else
      eina_value_util_stringshare_copy(&val, &azy_user_type_tmp->disc);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "song_pos", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    eina_value_get(&val, &azy_user_type_tmp->song_pos);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "songid", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    eina_value_get(&val, &azy_user_type_tmp->songid);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;

  *azy_user_type = azy_user_type_tmp;
  return EINA_TRUE;
}

Eina_Value *Array_Empd_Empdd_Song_to_azy_value(const Eina_List * azy_user_type)
{
  const Eina_List *l;
  Eina_Value_Struct st;
  Empd_Empdd_Song * v;
  Eina_Value *value_array = eina_value_array_new(EINA_VALUE_TYPE_STRUCT, 0);

  EINA_LIST_FOREACH(azy_user_type, l, v)
  {
    Eina_Value *item_value = Empd_Empdd_Song_to_azy_value((Empd_Empdd_Song *)v);

    eina_value_get(item_value, &st);
    eina_value_array_append(value_array, st);
    eina_value_free(item_value);
  }

  return value_array;
}

Eina_Bool azy_value_to_Array_Empd_Empdd_Song(const Eina_Value *value_array, Eina_List ** azy_user_type)
{
  Eina_List *azy_user_type_tmp = NULL;
  unsigned int x, total;

  EINA_SAFETY_ON_NULL_RETURN_VAL(azy_user_type, EINA_FALSE);

  if ((!value_array) || (eina_value_type_get(value_array) != EINA_VALUE_TYPE_ARRAY))
    return EINA_FALSE;

  total = eina_value_array_count(value_array);
  for (x = 0; x < total; x++)
  {
    Empd_Empdd_Song * item_value = NULL;
    Eina_Value val;

    if (eina_value_array_value_get(value_array, x, &val))
    {
      azy_value_to_Empd_Empdd_Song(&val, &item_value);
      eina_value_flush(&val);
    }

    azy_user_type_tmp = eina_list_append(azy_user_type_tmp, item_value);
  }

  *azy_user_type = azy_user_type_tmp;
  return EINA_TRUE;
}

Eina_Value *Empd_Array_Songs_to_azy_value(const Empd_Array_Songs * azy_user_type)
{
  Eina_Value *value_struct = NULL;
  size_t offset = 0;
  Eina_Value_Struct_Member *members = NULL;
  Eina_Value *val = NULL;
  Eina_Value_Struct_Desc *st_desc;

  if (!azy_user_type) return NULL;

  st_desc = eina_value_util_struct_desc_new();
  members = malloc(1 * sizeof(Eina_Value_Struct_Member));
  st_desc->members = members;
  st_desc->member_count = 1;
  members[0].name = eina_stringshare_add("songs");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_ARRAY, offset);
  members[0].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_ARRAY);
  members[0].type = EINA_VALUE_TYPE_ARRAY;
  st_desc->size = offset;
  value_struct = eina_value_struct_new(st_desc);

  val = Array_Empd_Empdd_Song_to_azy_value(azy_user_type->songs);
  eina_value_struct_value_set(value_struct, "songs", val);
  eina_value_free(val);
  return value_struct;
}

Eina_Bool azy_value_to_Empd_Array_Songs(const Eina_Value *value_struct, Empd_Array_Songs * *azy_user_type)
{
  Empd_Array_Songs * azy_user_type_tmp = NULL;
  Eina_Value val;
  unsigned int arg = 0;
  Eina_Bool found = EINA_FALSE;

  EINA_SAFETY_ON_NULL_RETURN_VAL(azy_user_type, EINA_FALSE);
  EINA_SAFETY_ON_NULL_RETURN_VAL(value_struct, EINA_FALSE);

  EINA_SAFETY_ON_TRUE_RETURN_VAL(eina_value_type_get(value_struct) != EINA_VALUE_TYPE_STRUCT, EINA_FALSE);

  azy_user_type_tmp = Empd_Array_Songs_new();
  if ((!arg) && eina_value_struct_value_get(value_struct, "songs", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    azy_value_to_Array_Empd_Empdd_Song(&val, &azy_user_type_tmp->songs);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;

  *azy_user_type = azy_user_type_tmp;
  return EINA_TRUE;
}

Eina_Value *Empd_Empdd_File_to_azy_value(const Empd_Empdd_File * azy_user_type)
{
  Eina_Value *value_struct = NULL;
  size_t offset = 0;
  Eina_Value_Struct_Member *members = NULL;
  Eina_Value *val = NULL;
  Eina_Value_Struct_Desc *st_desc;

  if (!azy_user_type) return NULL;

  st_desc = eina_value_util_struct_desc_new();
  members = malloc(10 * sizeof(Eina_Value_Struct_Member));
  st_desc->members = members;
  st_desc->member_count = 10;
  members[0].name = eina_stringshare_add("uri");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_STRINGSHARE, offset);
  members[0].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_STRINGSHARE);
  members[0].type = EINA_VALUE_TYPE_STRINGSHARE;
  members[1].name = eina_stringshare_add("last_modified");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_TIMESTAMP, offset);
  members[1].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_TIMESTAMP);
  members[1].type = EINA_VALUE_TYPE_TIMESTAMP;
  members[2].name = eina_stringshare_add("duration");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_TIMESTAMP, offset);
  members[2].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_TIMESTAMP);
  members[2].type = EINA_VALUE_TYPE_TIMESTAMP;
  members[3].name = eina_stringshare_add("artist");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_STRINGSHARE, offset);
  members[3].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_STRINGSHARE);
  members[3].type = EINA_VALUE_TYPE_STRINGSHARE;
  members[4].name = eina_stringshare_add("title");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_STRINGSHARE, offset);
  members[4].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_STRINGSHARE);
  members[4].type = EINA_VALUE_TYPE_STRINGSHARE;
  members[5].name = eina_stringshare_add("album");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_STRINGSHARE, offset);
  members[5].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_STRINGSHARE);
  members[5].type = EINA_VALUE_TYPE_STRINGSHARE;
  members[6].name = eina_stringshare_add("track");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_INT, offset);
  members[6].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_INT);
  members[6].type = EINA_VALUE_TYPE_INT;
  members[7].name = eina_stringshare_add("name");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_STRINGSHARE, offset);
  members[7].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_STRINGSHARE);
  members[7].type = EINA_VALUE_TYPE_STRINGSHARE;
  members[8].name = eina_stringshare_add("date");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_STRINGSHARE, offset);
  members[8].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_STRINGSHARE);
  members[8].type = EINA_VALUE_TYPE_STRINGSHARE;
  members[9].name = eina_stringshare_add("disc");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_STRINGSHARE, offset);
  members[9].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_STRINGSHARE);
  members[9].type = EINA_VALUE_TYPE_STRINGSHARE;
  st_desc->size = offset;
  value_struct = eina_value_struct_new(st_desc);

  val = eina_value_util_stringshare_new(azy_user_type->uri);
  eina_value_struct_value_set(value_struct, "uri", val);
  eina_value_free(val);
  val = eina_value_util_time_new(azy_user_type->last_modified);
  eina_value_struct_value_set(value_struct, "last_modified", val);
  eina_value_free(val);
  val = eina_value_util_time_new(azy_user_type->duration);
  eina_value_struct_value_set(value_struct, "duration", val);
  eina_value_free(val);
  val = eina_value_util_stringshare_new(azy_user_type->artist);
  eina_value_struct_value_set(value_struct, "artist", val);
  eina_value_free(val);
  val = eina_value_util_stringshare_new(azy_user_type->title);
  eina_value_struct_value_set(value_struct, "title", val);
  eina_value_free(val);
  val = eina_value_util_stringshare_new(azy_user_type->album);
  eina_value_struct_value_set(value_struct, "album", val);
  eina_value_free(val);
  val = eina_value_util_int_new(azy_user_type->track);
  eina_value_struct_value_set(value_struct, "track", val);
  eina_value_free(val);
  val = eina_value_util_stringshare_new(azy_user_type->name);
  eina_value_struct_value_set(value_struct, "name", val);
  eina_value_free(val);
  val = eina_value_util_stringshare_new(azy_user_type->date);
  eina_value_struct_value_set(value_struct, "date", val);
  eina_value_free(val);
  val = eina_value_util_stringshare_new(azy_user_type->disc);
  eina_value_struct_value_set(value_struct, "disc", val);
  eina_value_free(val);
  return value_struct;
}

Eina_Bool azy_value_to_Empd_Empdd_File(const Eina_Value *value_struct, Empd_Empdd_File * *azy_user_type)
{
  Empd_Empdd_File * azy_user_type_tmp = NULL;
  Eina_Value val;
  unsigned int arg = 0;
  Eina_Bool found = EINA_FALSE;

  EINA_SAFETY_ON_NULL_RETURN_VAL(azy_user_type, EINA_FALSE);
  EINA_SAFETY_ON_NULL_RETURN_VAL(value_struct, EINA_FALSE);

  EINA_SAFETY_ON_TRUE_RETURN_VAL(eina_value_type_get(value_struct) != EINA_VALUE_TYPE_STRUCT, EINA_FALSE);

  azy_user_type_tmp = Empd_Empdd_File_new();
  if ((!arg) && eina_value_struct_value_get(value_struct, "uri", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    if (arg)
      {
        char *str = NULL;

        if (eina_value_get(&val, &str))
          azy_user_type_tmp->uri = eina_stringshare_add(str);
      }
    else
      eina_value_util_stringshare_copy(&val, &azy_user_type_tmp->uri);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "last_modified", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    eina_value_get(&val, &azy_user_type_tmp->last_modified);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "duration", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    eina_value_get(&val, &azy_user_type_tmp->duration);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "artist", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    if (arg)
      {
        char *str = NULL;

        if (eina_value_get(&val, &str))
          azy_user_type_tmp->artist = eina_stringshare_add(str);
      }
    else
      eina_value_util_stringshare_copy(&val, &azy_user_type_tmp->artist);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "title", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    if (arg)
      {
        char *str = NULL;

        if (eina_value_get(&val, &str))
          azy_user_type_tmp->title = eina_stringshare_add(str);
      }
    else
      eina_value_util_stringshare_copy(&val, &azy_user_type_tmp->title);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "album", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    if (arg)
      {
        char *str = NULL;

        if (eina_value_get(&val, &str))
          azy_user_type_tmp->album = eina_stringshare_add(str);
      }
    else
      eina_value_util_stringshare_copy(&val, &azy_user_type_tmp->album);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "track", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    eina_value_get(&val, &azy_user_type_tmp->track);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "name", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    if (arg)
      {
        char *str = NULL;

        if (eina_value_get(&val, &str))
          azy_user_type_tmp->name = eina_stringshare_add(str);
      }
    else
      eina_value_util_stringshare_copy(&val, &azy_user_type_tmp->name);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "date", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    if (arg)
      {
        char *str = NULL;

        if (eina_value_get(&val, &str))
          azy_user_type_tmp->date = eina_stringshare_add(str);
      }
    else
      eina_value_util_stringshare_copy(&val, &azy_user_type_tmp->date);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "disc", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    if (arg)
      {
        char *str = NULL;

        if (eina_value_get(&val, &str))
          azy_user_type_tmp->disc = eina_stringshare_add(str);
      }
    else
      eina_value_util_stringshare_copy(&val, &azy_user_type_tmp->disc);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;

  *azy_user_type = azy_user_type_tmp;
  return EINA_TRUE;
}

Eina_Value *Empd_Empdd_Directory_to_azy_value(const Empd_Empdd_Directory * azy_user_type)
{
  Eina_Value *value_struct = NULL;
  size_t offset = 0;
  Eina_Value_Struct_Member *members = NULL;
  Eina_Value *val = NULL;
  Eina_Value_Struct_Desc *st_desc;

  if (!azy_user_type) return NULL;

  st_desc = eina_value_util_struct_desc_new();
  members = malloc(2 * sizeof(Eina_Value_Struct_Member));
  st_desc->members = members;
  st_desc->member_count = 2;
  members[0].name = eina_stringshare_add("uri");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_STRINGSHARE, offset);
  members[0].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_STRINGSHARE);
  members[0].type = EINA_VALUE_TYPE_STRINGSHARE;
  members[1].name = eina_stringshare_add("last_modified");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_TIMESTAMP, offset);
  members[1].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_TIMESTAMP);
  members[1].type = EINA_VALUE_TYPE_TIMESTAMP;
  st_desc->size = offset;
  value_struct = eina_value_struct_new(st_desc);

  val = eina_value_util_stringshare_new(azy_user_type->uri);
  eina_value_struct_value_set(value_struct, "uri", val);
  eina_value_free(val);
  val = eina_value_util_time_new(azy_user_type->last_modified);
  eina_value_struct_value_set(value_struct, "last_modified", val);
  eina_value_free(val);
  return value_struct;
}

Eina_Bool azy_value_to_Empd_Empdd_Directory(const Eina_Value *value_struct, Empd_Empdd_Directory * *azy_user_type)
{
  Empd_Empdd_Directory * azy_user_type_tmp = NULL;
  Eina_Value val;
  unsigned int arg = 0;
  Eina_Bool found = EINA_FALSE;

  EINA_SAFETY_ON_NULL_RETURN_VAL(azy_user_type, EINA_FALSE);
  EINA_SAFETY_ON_NULL_RETURN_VAL(value_struct, EINA_FALSE);

  EINA_SAFETY_ON_TRUE_RETURN_VAL(eina_value_type_get(value_struct) != EINA_VALUE_TYPE_STRUCT, EINA_FALSE);

  azy_user_type_tmp = Empd_Empdd_Directory_new();
  if ((!arg) && eina_value_struct_value_get(value_struct, "uri", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    if (arg)
      {
        char *str = NULL;

        if (eina_value_get(&val, &str))
          azy_user_type_tmp->uri = eina_stringshare_add(str);
      }
    else
      eina_value_util_stringshare_copy(&val, &azy_user_type_tmp->uri);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "last_modified", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    eina_value_get(&val, &azy_user_type_tmp->last_modified);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;

  *azy_user_type = azy_user_type_tmp;
  return EINA_TRUE;
}

Eina_Value *Empd_Empdd_Playlist_to_azy_value(const Empd_Empdd_Playlist * azy_user_type)
{
  Eina_Value *value_struct = NULL;
  size_t offset = 0;
  Eina_Value_Struct_Member *members = NULL;
  Eina_Value *val = NULL;
  Eina_Value_Struct_Desc *st_desc;

  if (!azy_user_type) return NULL;

  st_desc = eina_value_util_struct_desc_new();
  members = malloc(2 * sizeof(Eina_Value_Struct_Member));
  st_desc->members = members;
  st_desc->member_count = 2;
  members[0].name = eina_stringshare_add("uri");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_STRINGSHARE, offset);
  members[0].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_STRINGSHARE);
  members[0].type = EINA_VALUE_TYPE_STRINGSHARE;
  members[1].name = eina_stringshare_add("last_modified");
  offset = eina_value_util_type_offset(EINA_VALUE_TYPE_TIMESTAMP, offset);
  members[1].offset = offset;
  offset += eina_value_util_type_size(EINA_VALUE_TYPE_TIMESTAMP);
  members[1].type = EINA_VALUE_TYPE_TIMESTAMP;
  st_desc->size = offset;
  value_struct = eina_value_struct_new(st_desc);

  val = eina_value_util_stringshare_new(azy_user_type->uri);
  eina_value_struct_value_set(value_struct, "uri", val);
  eina_value_free(val);
  val = eina_value_util_time_new(azy_user_type->last_modified);
  eina_value_struct_value_set(value_struct, "last_modified", val);
  eina_value_free(val);
  return value_struct;
}

Eina_Bool azy_value_to_Empd_Empdd_Playlist(const Eina_Value *value_struct, Empd_Empdd_Playlist * *azy_user_type)
{
  Empd_Empdd_Playlist * azy_user_type_tmp = NULL;
  Eina_Value val;
  unsigned int arg = 0;
  Eina_Bool found = EINA_FALSE;

  EINA_SAFETY_ON_NULL_RETURN_VAL(azy_user_type, EINA_FALSE);
  EINA_SAFETY_ON_NULL_RETURN_VAL(value_struct, EINA_FALSE);

  EINA_SAFETY_ON_TRUE_RETURN_VAL(eina_value_type_get(value_struct) != EINA_VALUE_TYPE_STRUCT, EINA_FALSE);

  azy_user_type_tmp = Empd_Empdd_Playlist_new();
  if ((!arg) && eina_value_struct_value_get(value_struct, "uri", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    if (arg)
      {
        char *str = NULL;

        if (eina_value_get(&val, &str))
          azy_user_type_tmp->uri = eina_stringshare_add(str);
      }
    else
      eina_value_util_stringshare_copy(&val, &azy_user_type_tmp->uri);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;
  if ((!arg) && eina_value_struct_value_get(value_struct, "last_modified", &val))
    found = EINA_TRUE;
  else
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "arg%d", arg++);
    if (eina_value_struct_value_get(value_struct, buf, &val))
      found = EINA_TRUE;
  }
  if (found)
  {
    eina_value_get(&val, &azy_user_type_tmp->last_modified);
    eina_value_flush(&val);
  }
  found = EINA_FALSE;

  *azy_user_type = azy_user_type_tmp;
  return EINA_TRUE;
}

