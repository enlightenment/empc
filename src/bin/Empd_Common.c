#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include "Empd_Common.h"
#include <string.h>
#include <inttypes.h>
#include <errno.h>

Eina_Bool
azy_str_to_bool_(const char *d, Eina_Bool *ret)
{
  *ret = EINA_FALSE;
  if (d && (*d == '1')) *ret = EINA_TRUE;
  return EINA_TRUE;
}

Eina_Bool
azy_str_to_str_(const char *d, const char **ret)
{
  *ret = NULL;
  if (!d) return EINA_TRUE;
  *ret = eina_stringshare_add(d);
  return EINA_TRUE;
}

Eina_Bool
azy_str_to_int_(const char *d, int *ret)
{
  errno = 0;
  *ret = 0;
  if (!d) return EINA_TRUE;
  *ret = strtol(d, NULL, 10);
  if (errno)
    {
#ifdef ERR
      fprintf(stderr, "Error converting %s to int: '%s'", d, strerror(errno));
#endif
      return EINA_FALSE;
    }
  return EINA_TRUE;
}

Eina_Bool
azy_str_to_double_(const char *d, double *ret)
{
  errno = 0;
  *ret = 0.0;
  if (!d) return EINA_TRUE;
  *ret = strtod(d, NULL);
  if (errno)
    {
#ifdef ERR
      fprintf(stderr, "Error converting %s to double: '%s'", d, strerror(errno));
#endif
      return EINA_FALSE;
    }
  return EINA_TRUE;
}
void Empd_Empdd_Song_free(Empd_Empdd_Song * val)
{
  if (!val)
    return;

  eina_stringshare_del(val->uri);
  eina_stringshare_del(val->artist);
  eina_stringshare_del(val->title);
  eina_stringshare_del(val->album);
  eina_stringshare_del(val->name);
  eina_stringshare_del(val->date);
  eina_stringshare_del(val->disc);
  free(val);
}

Empd_Empdd_Song * Empd_Empdd_Song_copy(Empd_Empdd_Song * orig)
{
  Empd_Empdd_Song * copy;

  if (!orig)
    return NULL;

  copy = Empd_Empdd_Song_new();
  copy->uri = eina_stringshare_ref(orig->uri);
  copy->last_modified = orig->last_modified;
  copy->duration = orig->duration;
  copy->artist = eina_stringshare_ref(orig->artist);
  copy->title = eina_stringshare_ref(orig->title);
  copy->album = eina_stringshare_ref(orig->album);
  copy->track = orig->track;
  copy->name = eina_stringshare_ref(orig->name);
  copy->date = eina_stringshare_ref(orig->date);
  copy->disc = eina_stringshare_ref(orig->disc);
  copy->song_pos = orig->song_pos;
  copy->songid = orig->songid;

  return copy;
}

Eina_Bool Empd_Empdd_Song_eq(Empd_Empdd_Song * a, Empd_Empdd_Song * b)
{
  if (a == b)
    return EINA_TRUE;
  if ((!a) || (!b))
    return EINA_FALSE;
  if (a->uri != b->uri)
    return EINA_FALSE;
  if (a->last_modified != b->last_modified)
    return EINA_FALSE;
  if (a->duration != b->duration)
    return EINA_FALSE;
  if (a->artist != b->artist)
    return EINA_FALSE;
  if (a->title != b->title)
    return EINA_FALSE;
  if (a->album != b->album)
    return EINA_FALSE;
  if (a->track != b->track)
    return EINA_FALSE;
  if (a->name != b->name)
    return EINA_FALSE;
  if (a->date != b->date)
    return EINA_FALSE;
  if (a->disc != b->disc)
    return EINA_FALSE;
  if (a->song_pos != b->song_pos)
    return EINA_FALSE;
  if (a->songid != b->songid)
    return EINA_FALSE;

  return EINA_TRUE;
}

void Empd_Empdd_Song_print(const char *pre, int indent, const Empd_Empdd_Song *a)
{
  int i;
  if (!a)
    return;
  if (!pre) pre = "\t";
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("uri: %s\n", a->uri);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("last_modified: %ld\n", a->last_modified);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("duration: %ld\n", a->duration);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("artist: %s\n", a->artist);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("title: %s\n", a->title);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("album: %s\n", a->album);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("track: %i\n", a->track);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("name: %s\n", a->name);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("date: %s\n", a->date);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("disc: %s\n", a->disc);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("song_pos: %i\n", a->song_pos);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("songid: %i\n", a->songid);
}

Eina_Bool Empd_Empdd_Song_isnull(Empd_Empdd_Song * a)
{
  if (!a)
    return EINA_TRUE;
  if (a->uri != NULL)
    return EINA_FALSE;
  if (a->last_modified != 0)
    return EINA_FALSE;
  if (a->duration != 0)
    return EINA_FALSE;
  if (a->artist != NULL)
    return EINA_FALSE;
  if (a->title != NULL)
    return EINA_FALSE;
  if (a->album != NULL)
    return EINA_FALSE;
  if (a->track != 0)
    return EINA_FALSE;
  if (a->name != NULL)
    return EINA_FALSE;
  if (a->date != NULL)
    return EINA_FALSE;
  if (a->disc != NULL)
    return EINA_FALSE;
  if (a->song_pos != 0)
    return EINA_FALSE;
  if (a->songid != 0)
    return EINA_FALSE;

  return EINA_TRUE;
}

void Array_Empd_Empdd_Song_free(Eina_List *val)
{
  Empd_Empdd_Song * t;
  if (!val) return;
  EINA_LIST_FREE(val, t)
    Empd_Empdd_Song_free(t);
}

Eina_List *Array_Empd_Empdd_Song_copy(Eina_List *orig)
{
  Eina_List *copy = NULL;
  Eina_List *l;
  Empd_Empdd_Song * t;

  EINA_LIST_FOREACH(orig, l, t)
    copy = eina_list_append(copy, Empd_Empdd_Song_copy((Empd_Empdd_Song *)t));

  return copy;
}

Eina_Bool Array_Empd_Empdd_Song_eq(Eina_List * a, Eina_List * b)
{
  Eina_List *y, *z;

  if (a == b)
    return EINA_TRUE;
  if ((!a) || (!b))
    return EINA_FALSE;
  for (y = a, z = b;y && z; y = y->next, z = z->next)
  {
    if (!Empd_Empdd_Song_eq(y->data, z->data))
      return EINA_FALSE;
  }

  return EINA_TRUE;
}

void Array_Empd_Empdd_Song_print(const char *pre, int indent, const Eina_List *a)
{
  const Eina_List *l;
  Empd_Empdd_Song * t;

  if (!a)
    return;
  EINA_LIST_FOREACH(a, l, t)
    Empd_Empdd_Song_print(pre, indent + 1, t);
}

void Empd_Array_Songs_free(Empd_Array_Songs * val)
{
  if (!val)
    return;

  Array_Empd_Empdd_Song_free(val->songs);
  free(val);
}

Empd_Array_Songs * Empd_Array_Songs_copy(Empd_Array_Songs * orig)
{
  Empd_Array_Songs * copy;

  if (!orig)
    return NULL;

  copy = Empd_Array_Songs_new();
  copy->songs = Array_Empd_Empdd_Song_copy(orig->songs);

  return copy;
}

Eina_Bool Empd_Array_Songs_eq(Empd_Array_Songs * a, Empd_Array_Songs * b)
{
  if (a == b)
    return EINA_TRUE;
  if ((!a) || (!b))
    return EINA_FALSE;
  if (!Array_Empd_Empdd_Song_eq(a->songs, b->songs))
    return EINA_FALSE;

  return EINA_TRUE;
}

void Empd_Array_Songs_print(const char *pre, int indent, const Empd_Array_Songs *a)
{
  int i;
  if (!a)
    return;
  if (!pre) pre = "\t";
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("songs:\n");
  Array_Empd_Empdd_Song_print(pre, indent + 1, a->songs);
  printf("\n");
}

Eina_Bool Empd_Array_Songs_isnull(Empd_Array_Songs * a)
{
  if (!a)
    return EINA_TRUE;
  if (a->songs != NULL)
    return EINA_FALSE;

  return EINA_TRUE;
}

void Empd_Empdd_File_free(Empd_Empdd_File * val)
{
  if (!val)
    return;

  eina_stringshare_del(val->uri);
  eina_stringshare_del(val->artist);
  eina_stringshare_del(val->title);
  eina_stringshare_del(val->album);
  eina_stringshare_del(val->name);
  eina_stringshare_del(val->date);
  eina_stringshare_del(val->disc);
  free(val);
}

Empd_Empdd_File * Empd_Empdd_File_copy(Empd_Empdd_File * orig)
{
  Empd_Empdd_File * copy;

  if (!orig)
    return NULL;

  copy = Empd_Empdd_File_new();
  copy->uri = eina_stringshare_ref(orig->uri);
  copy->last_modified = orig->last_modified;
  copy->duration = orig->duration;
  copy->artist = eina_stringshare_ref(orig->artist);
  copy->title = eina_stringshare_ref(orig->title);
  copy->album = eina_stringshare_ref(orig->album);
  copy->track = orig->track;
  copy->name = eina_stringshare_ref(orig->name);
  copy->date = eina_stringshare_ref(orig->date);
  copy->disc = eina_stringshare_ref(orig->disc);

  return copy;
}

Eina_Bool Empd_Empdd_File_eq(Empd_Empdd_File * a, Empd_Empdd_File * b)
{
  if (a == b)
    return EINA_TRUE;
  if ((!a) || (!b))
    return EINA_FALSE;
  if (a->uri != b->uri)
    return EINA_FALSE;
  if (a->last_modified != b->last_modified)
    return EINA_FALSE;
  if (a->duration != b->duration)
    return EINA_FALSE;
  if (a->artist != b->artist)
    return EINA_FALSE;
  if (a->title != b->title)
    return EINA_FALSE;
  if (a->album != b->album)
    return EINA_FALSE;
  if (a->track != b->track)
    return EINA_FALSE;
  if (a->name != b->name)
    return EINA_FALSE;
  if (a->date != b->date)
    return EINA_FALSE;
  if (a->disc != b->disc)
    return EINA_FALSE;

  return EINA_TRUE;
}

void Empd_Empdd_File_print(const char *pre, int indent, const Empd_Empdd_File *a)
{
  int i;
  if (!a)
    return;
  if (!pre) pre = "\t";
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("uri: %s\n", a->uri);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("last_modified: %ld\n", a->last_modified);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("duration: %ld\n", a->duration);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("artist: %s\n", a->artist);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("title: %s\n", a->title);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("album: %s\n", a->album);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("track: %i\n", a->track);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("name: %s\n", a->name);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("date: %s\n", a->date);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("disc: %s\n", a->disc);
}

Eina_Bool Empd_Empdd_File_isnull(Empd_Empdd_File * a)
{
  if (!a)
    return EINA_TRUE;
  if (a->uri != NULL)
    return EINA_FALSE;
  if (a->last_modified != 0)
    return EINA_FALSE;
  if (a->duration != 0)
    return EINA_FALSE;
  if (a->artist != NULL)
    return EINA_FALSE;
  if (a->title != NULL)
    return EINA_FALSE;
  if (a->album != NULL)
    return EINA_FALSE;
  if (a->track != 0)
    return EINA_FALSE;
  if (a->name != NULL)
    return EINA_FALSE;
  if (a->date != NULL)
    return EINA_FALSE;
  if (a->disc != NULL)
    return EINA_FALSE;

  return EINA_TRUE;
}

void Empd_Empdd_Directory_free(Empd_Empdd_Directory * val)
{
  if (!val)
    return;

  eina_stringshare_del(val->uri);
  free(val);
}

Empd_Empdd_Directory * Empd_Empdd_Directory_copy(Empd_Empdd_Directory * orig)
{
  Empd_Empdd_Directory * copy;

  if (!orig)
    return NULL;

  copy = Empd_Empdd_Directory_new();
  copy->uri = eina_stringshare_ref(orig->uri);
  copy->last_modified = orig->last_modified;

  return copy;
}

Eina_Bool Empd_Empdd_Directory_eq(Empd_Empdd_Directory * a, Empd_Empdd_Directory * b)
{
  if (a == b)
    return EINA_TRUE;
  if ((!a) || (!b))
    return EINA_FALSE;
  if (a->uri != b->uri)
    return EINA_FALSE;
  if (a->last_modified != b->last_modified)
    return EINA_FALSE;

  return EINA_TRUE;
}

void Empd_Empdd_Directory_print(const char *pre, int indent, const Empd_Empdd_Directory *a)
{
  int i;
  if (!a)
    return;
  if (!pre) pre = "\t";
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("uri: %s\n", a->uri);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("last_modified: %ld\n", a->last_modified);
}

Eina_Bool Empd_Empdd_Directory_isnull(Empd_Empdd_Directory * a)
{
  if (!a)
    return EINA_TRUE;
  if (a->uri != NULL)
    return EINA_FALSE;
  if (a->last_modified != 0)
    return EINA_FALSE;

  return EINA_TRUE;
}

void Empd_Empdd_Playlist_free(Empd_Empdd_Playlist * val)
{
  if (!val)
    return;

  eina_stringshare_del(val->uri);
  free(val);
}

Empd_Empdd_Playlist * Empd_Empdd_Playlist_copy(Empd_Empdd_Playlist * orig)
{
  Empd_Empdd_Playlist * copy;

  if (!orig)
    return NULL;

  copy = Empd_Empdd_Playlist_new();
  copy->uri = eina_stringshare_ref(orig->uri);
  copy->last_modified = orig->last_modified;

  return copy;
}

Eina_Bool Empd_Empdd_Playlist_eq(Empd_Empdd_Playlist * a, Empd_Empdd_Playlist * b)
{
  if (a == b)
    return EINA_TRUE;
  if ((!a) || (!b))
    return EINA_FALSE;
  if (a->uri != b->uri)
    return EINA_FALSE;
  if (a->last_modified != b->last_modified)
    return EINA_FALSE;

  return EINA_TRUE;
}

void Empd_Empdd_Playlist_print(const char *pre, int indent, const Empd_Empdd_Playlist *a)
{
  int i;
  if (!a)
    return;
  if (!pre) pre = "\t";
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("uri: %s\n", a->uri);
  for (i = 0; i < indent; i++)
    printf("%s", pre);
  printf("last_modified: %ld\n", a->last_modified);
}

Eina_Bool Empd_Empdd_Playlist_isnull(Empd_Empdd_Playlist * a)
{
  if (!a)
    return EINA_TRUE;
  if (a->uri != NULL)
    return EINA_FALSE;
  if (a->last_modified != 0)
    return EINA_FALSE;

  return EINA_TRUE;
}

