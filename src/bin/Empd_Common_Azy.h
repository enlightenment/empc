#ifndef Empd_Common_AZY_H
#define Empd_Common_AZY_H

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <Eina.h>
#include "Empd_Common.h"

Eina_Value *Empd_Empdd_Song_to_azy_value(const Empd_Empdd_Song * azy_user_type) EINA_WARN_UNUSED_RESULT;
Eina_Bool azy_value_to_Empd_Empdd_Song(const Eina_Value *value_struct, Empd_Empdd_Song ** azy_user_type);
Eina_Value *Array_Empd_Empdd_Song_to_azy_value(const Eina_List * azy_user_type) EINA_WARN_UNUSED_RESULT;
Eina_Bool azy_value_to_Array_Empd_Empdd_Song(const Eina_Value *value_array, Eina_List ** azy_user_type);
Eina_Value *Empd_Array_Songs_to_azy_value(const Empd_Array_Songs * azy_user_type) EINA_WARN_UNUSED_RESULT;
Eina_Bool azy_value_to_Empd_Array_Songs(const Eina_Value *value_struct, Empd_Array_Songs ** azy_user_type);
Eina_Value *Empd_Empdd_File_to_azy_value(const Empd_Empdd_File * azy_user_type) EINA_WARN_UNUSED_RESULT;
Eina_Bool azy_value_to_Empd_Empdd_File(const Eina_Value *value_struct, Empd_Empdd_File ** azy_user_type);
Eina_Value *Empd_Empdd_Directory_to_azy_value(const Empd_Empdd_Directory * azy_user_type) EINA_WARN_UNUSED_RESULT;
Eina_Bool azy_value_to_Empd_Empdd_Directory(const Eina_Value *value_struct, Empd_Empdd_Directory ** azy_user_type);
Eina_Value *Empd_Empdd_Playlist_to_azy_value(const Empd_Empdd_Playlist * azy_user_type) EINA_WARN_UNUSED_RESULT;
Eina_Bool azy_value_to_Empd_Empdd_Playlist(const Eina_Value *value_struct, Empd_Empdd_Playlist ** azy_user_type);

#endif
