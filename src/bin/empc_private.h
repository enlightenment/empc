#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include "empdd.h"
#include <Eldbus.h>
#include <Efreet.h>
#include <Elementary.h>
#include "empc.h"
#include "empc_parts.h"

#define DBG(...)            EINA_LOG_DOM_DBG(empc_log_dom, __VA_ARGS__)
#define INF(...)            EINA_LOG_DOM_INFO(empc_log_dom, __VA_ARGS__)
#define WRN(...)            EINA_LOG_DOM_WARN(empc_log_dom, __VA_ARGS__)
#define ERR(...)            EINA_LOG_DOM_ERR(empc_log_dom, __VA_ARGS__)
#define CRI(...)            EINA_LOG_DOM_CRIT(empc_log_dom, __VA_ARGS__)

typedef struct Empc_Fetch_Request_End_Cb
{
   EINA_INLIST;
   Ecore_End_Cb cb;
   void *data;
} Empc_Fetch_Request_End_Cb;

typedef struct Empc_Module
{
   EINA_INLIST;
   int priority; //lower = better
   Empc_Module_Type type;
   Eina_Module *module;
   Eina_Bool remote : 1;
} Empc_Module;

typedef struct Empc_Module_Metadata_Fetch
{
   Empc_Module module;
   Empc_Module_Metadata_Fetch_Cb fetch;
   Empc_Module_Metadata_Cancel_Cb cancel;
} Empc_Module_Metadata_Fetch;

typedef struct Empc_Module_Metadata_Save
{
   Empc_Module module;
   Empc_Module_Metadata_Save_Image_Cb save_image;
   Empc_Module_Metadata_Save_Text_Cb save_text;
} Empc_Module_Metadata_Save;

typedef struct Empc_Module_Misc
{
   Empc_Module module;
} Empc_Module_Misc;

typedef struct Empc_Metadata_Result_Cb
{
   Empc_Module_Metadata_Fetch_Result_Cb cb;
   void *data;
   Evas_Object *obj;
   Eina_Bool force : 1;
} Empc_Metadata_Result_Cb;

typedef struct Empc_Fetch_Request_Internal
{
   Empc_Fetch_Request req;
   Empc_Module *module;
   Eina_List *results; //Empc_Metadata_Result_Cb
   Eina_Inlist *urls; // Metadata_Image
   Eina_List *urls_pending; //Eina_Stringshare
   unsigned int count;
   Eina_Inlist *end_cbs; // Empc_Fetch_Request_End_Cb
   Ecore_Job *fail_job;
   Eina_Bool in_progress : 1;
   Eina_Bool deleted : 1;
} Empc_Fetch_Request_Internal;

extern int empc_log_dom;
extern Eina_Inlist *empc_modules[];
extern Eina_Hash *empc_metadata_fetch_reqs[];

Empc_Fetch_Request *metadata_fetch_begin(Empc_Metadata_Type type, Evas_Object *obj, const char *attr1, const char *attr2, const char *uri, Eina_Bool force, Eina_Bool local, Empc_Module_Metadata_Fetch_Result_Cb cb, const void *data);
void metadata_fetch_cancel(Empc_Fetch_Request *req, Evas_Object *obj, Empc_Module_Metadata_Fetch_Result_Cb cb, const void *data);
void metadata_fetch_cb_add(Empc_Fetch_Request *req, Ecore_End_Cb cb, const void *data);
void metadata_shutdown(void);

Evas_Object *bgselector_add(Evas_Object *parent);
void bgselector_image_max_recalc(Evas_Object *img);
void bgselector_image_add(Evas_Object *obj, Evas_Object *img);
void bgselector_next(Evas_Object *obj);
void bgselector_prev(Evas_Object *obj);
Evas_Object *bgselector_get(Evas_Object *obj);
void bgselector_clear(Evas_Object *obj);
void bgselector_prune(Evas_Object *obj);
void bgselector_cancel(Evas_Object *obj);
void bgselector_active_set(Evas_Object *obj, Eina_Bool active);
void bgselector_info_set(Evas_Object *obj, Eina_Stringshare *artist, Eina_Stringshare *album);
Eina_Stringshare *bgselector_artist_get(Evas_Object *obj);
Eina_Stringshare *bgselector_album_get(Evas_Object *obj);
