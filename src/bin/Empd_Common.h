#ifndef Empd_Common_H
#define Empd_Common_H

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <Eina.h>

Eina_Bool azy_str_to_bool_(const char *d, Eina_Bool *ret);
Eina_Bool azy_str_to_str_(const char *d, const char **ret);
Eina_Bool azy_str_to_int_(const char *d, int *ret);
Eina_Bool azy_str_to_double_(const char *d, double *ret);
typedef struct Empd_Empdd_Song Empd_Empdd_Song;
typedef struct Empd_Array_Songs Empd_Array_Songs;
typedef struct Empd_Empdd_File Empd_Empdd_File;
typedef struct Empd_Empdd_Directory Empd_Empdd_Directory;
typedef struct Empd_Empdd_Playlist Empd_Empdd_Playlist;

struct Empd_Empdd_Song
{
  Eina_Stringshare * uri; /*  */
  time_t last_modified; /*  */
  time_t duration; /*  */
  Eina_Stringshare * artist; /*  */
  Eina_Stringshare * title; /*  */
  Eina_Stringshare * album; /*  */
  int track; /*  */
  Eina_Stringshare * name; /*  */
  Eina_Stringshare * date; /*  */
  Eina_Stringshare * disc; /*  */
  int song_pos; /*  */
  int songid; /*  */
};

static inline Empd_Empdd_Song *Empd_Empdd_Song_new(void)
{
  return calloc(1, sizeof(Empd_Empdd_Song));
}

struct Empd_Array_Songs
{
  Eina_List * songs; /* Array_Empd_Empdd_Song */
};

static inline Empd_Array_Songs *Empd_Array_Songs_new(void)
{
  return calloc(1, sizeof(Empd_Array_Songs));
}

struct Empd_Empdd_File
{
  Eina_Stringshare * uri; /*  */
  time_t last_modified; /*  */
  time_t duration; /*  */
  Eina_Stringshare * artist; /*  */
  Eina_Stringshare * title; /*  */
  Eina_Stringshare * album; /*  */
  int track; /*  */
  Eina_Stringshare * name; /*  */
  Eina_Stringshare * date; /*  */
  Eina_Stringshare * disc; /*  */
};

static inline Empd_Empdd_File *Empd_Empdd_File_new(void)
{
  return calloc(1, sizeof(Empd_Empdd_File));
}

struct Empd_Empdd_Directory
{
  Eina_Stringshare * uri; /*  */
  time_t last_modified; /*  */
};

static inline Empd_Empdd_Directory *Empd_Empdd_Directory_new(void)
{
  return calloc(1, sizeof(Empd_Empdd_Directory));
}

struct Empd_Empdd_Playlist
{
  Eina_Stringshare * uri; /*  */
  time_t last_modified; /*  */
};

static inline Empd_Empdd_Playlist *Empd_Empdd_Playlist_new(void)
{
  return calloc(1, sizeof(Empd_Empdd_Playlist));
}

/** @brief Free a #Empd_Empdd_Song * */
void Empd_Empdd_Song_free(Empd_Empdd_Song * val);
/** @brief Copy a #Empd_Empdd_Song * */
Empd_Empdd_Song *Empd_Empdd_Song_copy(Empd_Empdd_Song *orig);
/** @brief Check whether all the values of @p a are equal to @p b */
Eina_Bool Empd_Empdd_Song_eq(Empd_Empdd_Song * a, Empd_Empdd_Song * b);
/** @brief Print, indenting @p indent times @p pre, a Empd_Empdd_Song */
void Empd_Empdd_Song_print(const char *pre, int indent, const Empd_Empdd_Song *a);
/** @brief Check whether all the values of @p a are NULL */
Eina_Bool Empd_Empdd_Song_isnull(Empd_Empdd_Song * a);
/** @brief Free a #Eina_List * */
void Array_Empd_Empdd_Song_free(Eina_List * val);
/** @brief Copy a #Eina_List * */
Eina_List *Array_Empd_Empdd_Song_copy(Eina_List *orig);
/** @brief Check whether all the values of @p a are equal to @p b */
Eina_Bool Array_Empd_Empdd_Song_eq(Eina_List * a, Eina_List * b);
/** @brief Print, indenting @p indent times @p pre, an array of Array_Empd_Empdd_Song */
void Array_Empd_Empdd_Song_print(const char *pre, int indent, const Eina_List *a);
/** @brief Free a #Empd_Array_Songs * */
void Empd_Array_Songs_free(Empd_Array_Songs * val);
/** @brief Copy a #Empd_Array_Songs * */
Empd_Array_Songs *Empd_Array_Songs_copy(Empd_Array_Songs *orig);
/** @brief Check whether all the values of @p a are equal to @p b */
Eina_Bool Empd_Array_Songs_eq(Empd_Array_Songs * a, Empd_Array_Songs * b);
/** @brief Print, indenting @p indent times @p pre, a Empd_Array_Songs */
void Empd_Array_Songs_print(const char *pre, int indent, const Empd_Array_Songs *a);
/** @brief Check whether all the values of @p a are NULL */
Eina_Bool Empd_Array_Songs_isnull(Empd_Array_Songs * a);
/** @brief Free a #Empd_Empdd_File * */
void Empd_Empdd_File_free(Empd_Empdd_File * val);
/** @brief Copy a #Empd_Empdd_File * */
Empd_Empdd_File *Empd_Empdd_File_copy(Empd_Empdd_File *orig);
/** @brief Check whether all the values of @p a are equal to @p b */
Eina_Bool Empd_Empdd_File_eq(Empd_Empdd_File * a, Empd_Empdd_File * b);
/** @brief Print, indenting @p indent times @p pre, a Empd_Empdd_File */
void Empd_Empdd_File_print(const char *pre, int indent, const Empd_Empdd_File *a);
/** @brief Check whether all the values of @p a are NULL */
Eina_Bool Empd_Empdd_File_isnull(Empd_Empdd_File * a);
/** @brief Free a #Empd_Empdd_Directory * */
void Empd_Empdd_Directory_free(Empd_Empdd_Directory * val);
/** @brief Copy a #Empd_Empdd_Directory * */
Empd_Empdd_Directory *Empd_Empdd_Directory_copy(Empd_Empdd_Directory *orig);
/** @brief Check whether all the values of @p a are equal to @p b */
Eina_Bool Empd_Empdd_Directory_eq(Empd_Empdd_Directory * a, Empd_Empdd_Directory * b);
/** @brief Print, indenting @p indent times @p pre, a Empd_Empdd_Directory */
void Empd_Empdd_Directory_print(const char *pre, int indent, const Empd_Empdd_Directory *a);
/** @brief Check whether all the values of @p a are NULL */
Eina_Bool Empd_Empdd_Directory_isnull(Empd_Empdd_Directory * a);
/** @brief Free a #Empd_Empdd_Playlist * */
void Empd_Empdd_Playlist_free(Empd_Empdd_Playlist * val);
/** @brief Copy a #Empd_Empdd_Playlist * */
Empd_Empdd_Playlist *Empd_Empdd_Playlist_copy(Empd_Empdd_Playlist *orig);
/** @brief Check whether all the values of @p a are equal to @p b */
Eina_Bool Empd_Empdd_Playlist_eq(Empd_Empdd_Playlist * a, Empd_Empdd_Playlist * b);
/** @brief Print, indenting @p indent times @p pre, a Empd_Empdd_Playlist */
void Empd_Empdd_Playlist_print(const char *pre, int indent, const Empd_Empdd_Playlist *a);
/** @brief Check whether all the values of @p a are NULL */
Eina_Bool Empd_Empdd_Playlist_isnull(Empd_Empdd_Playlist * a);
#endif
