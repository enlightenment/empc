#ifndef EMPC_H
# define EMPC_H

#if defined(HAVE_GETTEXT) && defined(ENABLE_NLS)
#define _(string) gettext(string)
#else
#define _(string) (string)
#endif


#include <Eina.h>
#include "Empd_Common_Azy.h"
typedef enum
{
   EMPC_MODULE_TYPE_METADATA_FETCH,
   EMPC_MODULE_TYPE_METADATA_SAVER,
   EMPC_MODULE_TYPE_MISC,
   EMPC_MODULE_TYPE_LAST,
} Empc_Module_Type;

enum
{
   MPD_STATE_UNKNOWN = 0,
   MPD_STATE_STOP = 1,
   MPD_STATE_PLAY = 2,
   MPD_STATE_PAUSE = 3,
};

enum
{
   MPD_ENTITY_TYPE_UNKNOWN,
   MPD_ENTITY_TYPE_DIRECTORY,
   MPD_ENTITY_TYPE_SONG,
   MPD_ENTITY_TYPE_PLAYLIST,
};

#ifndef EMPC_EXTERNS_ONLY
#include <Evas.h>


typedef enum
{
   EMPC_METADATA_TYPE_IMAGE,
   EMPC_METADATA_TYPE_TEXT,
   EMPC_METADATA_TYPE_LAST,
} Empc_Metadata_Type;

typedef struct Empc_Fetch_Request Empc_Fetch_Request;

typedef Eina_Bool (*Empc_Module_Remote_Cb)(void);
typedef Empc_Module_Type (*Empc_Module_Type_Cb)(void);
typedef int (*Empc_Module_Priority_Cb)(void);
typedef void (*Empc_Module_Metadata_Save_Post_Cb)(void);
typedef void (*Empc_Module_Metadata_Save_Image_Cb)(Evas_Object *obj, const char *uri, const char *artist, const char *album, Empc_Module_Metadata_Save_Post_Cb post_cb);
typedef void (*Empc_Module_Metadata_Save_Text_Cb)(const char *text, const char *artist, const char *song);
typedef Eina_Bool (*Empc_Module_Metadata_Fetch_Cb)(Empc_Fetch_Request *);
typedef void (*Empc_Module_Metadata_Cancel_Cb)(Empc_Fetch_Request *);
typedef void (*Empc_Module_Metadata_Fetch_Result_Cb)(void *, Empc_Fetch_Request *, Evas_Object *img);

struct Empc_Fetch_Request
{
   Eina_Stringshare *artist;
   Eina_Stringshare *album;
   Eina_Stringshare *song;
   Eina_Stringshare *uri;
   Empc_Metadata_Type type;
   Eina_Bool local : 1; //only local fetches
   Eina_Bool force : 1;
   Eina_Bool running : 1; //module is active
};

void empc_metadata_fetch_done(Empc_Fetch_Request *req, Eina_List *l);
Eina_List *empc_metadata_entries_add(Empc_Fetch_Request *req);
Eina_List *empc_metadata_images_add(Empc_Fetch_Request *req);
void empc_metadata_image_download(Empc_Fetch_Request *req, const char *url);
void empc_metadata_image_download_queue(Empc_Fetch_Request *req, const char *url);
#endif
extern Eina_Stringshare *empd_music_directory;
extern unsigned int empd_queue_length;
extern Eina_Bool master;

EAPI extern void *empd_proxy;
EAPI extern void *empc_proxy;
EAPI extern Evas_Object *queue_list;
EAPI extern Eina_Hash *empd_current_queue;
EAPI extern Eina_Hash *empd_current_queue_headers;
EAPI Eina_Bool empc_modapi_queue_list_song_find(const Empd_Empdd_File *f);
EAPI const Eina_List *empc_modapi_queue_list_header_items_find(const char *artist, const char *album);

#endif
