#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "assert.h"
#include <Ecore.h>

# define E_FREE_FUNC(_h, _fn) do { _fn((void*)_h); _h = NULL; } while (0)
# define E_FREE(p)            do { free(p); p = NULL; } while (0)
# define E_FREE_LIST(list, free)    \
  do                                \
    {                               \
       void *_tmp_;                 \
       EINA_LIST_FREE(list, _tmp_) \
         {                          \
            free(_tmp_);            \
         }                          \
    }                               \
  while (0)

# define E_LIST_REVERSE_FREE(list, data)         \
  for (data = eina_list_last_data_get(list);          \
       list;                                     \
       list = eina_list_remove_list(list, eina_list_last(list)), \
       data = eina_list_last_data_get(list))

# define E_LIST_FOREACH(list, func)    \
  do                                \
    {                               \
       void *_tmp_;                 \
       const Eina_List *_list, *_list2;  \
       EINA_LIST_FOREACH_SAFE(list, _list, _list2, _tmp_) \
         {                          \
            func(_tmp_);            \
         }                          \
    }                               \
  while (0)

# define E_LIST_HANDLER_APPEND(list, type, callback, data) \
  do \
    { \
       Ecore_Event_Handler *_eh; \
       _eh = ecore_event_handler_add(type, (Ecore_Event_Handler_Cb)callback, data); \
       assert(_eh); \
       list = eina_list_append(list, _eh); \
    } \
  while (0)

#ifndef MAX
# define MAX(x, y) (((x) > (y)) ? (x) : (y))
#endif

#define DEFAULT_PORT 6600
#define EMPDD_METHOD_BASE "org.empd.empdd"

typedef enum
{
   EMPD_SEARCH_TYPE_FILE,
   EMPD_SEARCH_TYPE_KEY,
   EMPD_SEARCH_TYPE_BASE,
   EMPD_SEARCH_TYPE_MODIFIED,
   EMPD_SEARCH_TYPE_LAST
} Empd_Search_Type;
