bin_PROGRAMS += \
src/bin/empc \
src/bin/empdd

src_bin_empc_CPPFLAGS = \
$(AM_CFLAGS) \
-I$(top_builddir) \
-I$(top_builddir)/src/bin \
-I$(top_srcdir)/data/themes \
@EFL_CFLAGS@ \
@ELM_CFLAGS@ \
-DDATA_DIR=\"$(datadir)\" \
-DPACKAGE_DATA_DIR=\"$(datadir)/empc\" \
-DPACKAGE_LIB_DIR=\"$(libdir)\" \
-DPACKAGE_SRC_DIR=\"$(top_srcdir)\" \
-DEMPC_MODULE_PATH=\"$(libdir)/empc/$(MODULE_ARCH)\"

src_bin_empc_LDFLAGS = -rdynamic

src_bin_empc_LDADD = \
@EFL_LIBS@ \
@ELM_LIBS@

ELDBUS_H = \
src/bin/eldbus_empd_empdd.h \
src/bin/eldbus_empd_empc.h \
src/bin/eldbus_utils.h

ELDBUS_SRC = \
src/bin/eldbus_empd_empdd.c \
src/bin/eldbus_empd_empc.c \
$(ELDBUS_H)

AZY_H = \
src/bin/Empd_Common.h \
src/bin/Empd_Common_Azy.h

AZY_SRC = \
src/bin/Empd_Common.c \
src/bin/Empd_Common_Azy.c \
$(AZY_H)

src_bin_empc_SOURCES = \
src/bin/bgselector.c \
src/bin/empc.c \
src/bin/empc_metadata.c \
src/bin/empc.h \
src/bin/empc_private.h \
$(ELDBUS_SRC) \
$(AZY_SRC)

src/bin/empc.h: $(AZY_SRC) $(ELDBUS_SRC)

if HAVE_AZY_PARSER
$(AZY_SRC): src/bin/empdd.azy
	@AZY_PARSER@ -H -o $(top_builddir)/src/bin src/bin/empdd.azy
endif

$(ELDBUS_SRC): src/bin/empdd.xml
	@cd $(top_builddir)/src/bin && \
	eldbus-codegen $(abs_top_srcdir)/src/bin/empdd.xml

EXTRA_DIST += \
src/bin/empdd.xml \
src/bin/empdd.azy \
$(AZY_SRC) \
$(ELDBUS_SRC)

MAINTAINERCLEANFILES += $(ELDBUS_SRC)

if HAVE_AZY_PARSER
MAINTAINERCLEANFILES += $(AZY_SRC)
endif

src_bin_empdd_CPPFLAGS = \
$(AM_CFLAGS) \
-I$(top_builddir) \
@MPDCLIENT_CFLAGS@ \
@EFL_CFLAGS@ \
@ELM_CFLAGS@ \
-DDATA_DIR=\"$(datadir)\" \
-DPACKAGE_DATA_DIR=\"$(datadir)/empc\" \
-DPACKAGE_LIB_DIR=\"$(libdir)\" \
-DPACKAGE_SRC_DIR=\"$(top_srcdir)\" \
-DEMPC_MODULE_PATH=\"$(libdir)/empc/$(MODULE_ARCH)\"

src_bin_empdd_LDFLAGS = -rdynamic

src_bin_empdd_LDADD = \
@MPDCLIENT_LIBS@ \
@EFL_LIBS@ \
@ELM_LIBS@

src_bin_empdd_SOURCES = \
src/bin/empdd.c \
src/bin/empdd.h
