desktopdir = $(datadir)/applications
desktop_DATA = \
data/empc.desktop

icondir = $(datadir)/icons
icon_DATA = \
data/empc.png


EXTRA_DIST += $(desktop_DATA) $(icon_DATA)
