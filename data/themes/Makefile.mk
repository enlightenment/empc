EDJE_FLAGS = \
-id $(top_srcdir)/data/themes/img

empc_filesdir = $(datadir)/empc
empc_files_DATA = data/themes/empc.edj

images = \
data/themes/img/add.png \
data/themes/img/arrow-back.png \
data/themes/img/arrow_large_down.png \
data/themes/img/arrow_large_up.png \
data/themes/img/arrow_medium_down.png \
data/themes/img/arrow_medium_up.png \
data/themes/img/arrow_small_down.png \
data/themes/img/arrow_small_up.png \
data/themes/img/COPYING.icons \
data/themes/img/e-logo-blue-glow.png \
data/themes/img/entry_over.png \
data/themes/img/glow_bottom.png \
data/themes/img/glow_top.png \
data/themes/img/ibar_bg_v.png \
data/themes/img/playlist-consume.png \
data/themes/img/playlist-repeat.png \
data/themes/img/playlist-shuffle.png \
data/themes/img/playlist-single.png \
data/themes/img/replace.png \
data/themes/img/scrollbar_button_down1.png \
data/themes/img/scrollbar_button_left1.png \
data/themes/img/scrollbar_button_right1.png \
data/themes/img/scrollbar_button_up1.png \
data/themes/img/scrollbar_hdrag1.png \
data/themes/img/scrollbar_hdrag_thumb.png \
data/themes/img/scrollbar_vdrag1.png \
data/themes/img/scrollbar_vdrag_thumb.png \
data/themes/img/scroll_border_h.png \
data/themes/img/scroll_border_v.png \
data/themes/img/slider_bar.png \
data/themes/img/white_bar_vert_glow.png

EDC = \
data/themes/bgselector.edc \
data/themes/elm.edc \
data/themes/empc.edc \
data/themes/scroller.edc

EXTRA_DIST += \
$(EDC) \
$(images) \
data/themes/empc_parts.h

CLEANFILES += data/themes/empc.edj

data/themes/empc.edj: $(images) $(EDC)
	@MKDIR_P@ data/themes && \
	@edje_cc@ $(EDJE_FLAGS) \
	$(top_srcdir)/data/themes/empc.edc \
	data/themes/empc.edj
