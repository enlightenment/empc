#define EMPC_SWALLOW_BACKGROUND "empc.swallow.background"
#define EMPC_SWALLOW_LYRICS "empc.swallow.lyrics"

#define EMPC_TEXT_ARTIST "empc.text.artist"
#define EMPC_TEXT_ALBUM "empc.text.album"
#define EMPC_TEXT_TITLE "empc.text.title"

#define EMPC_SWALLOW_ARTIST "empc.swallow.artist"
#define EMPC_SWALLOW_ALBUM "empc.swallow.album"

#define EMPC_SWALLOW_POSITION "empc.swallow.position"
#define EMPC_SWALLOW_PLAYLIST "empc.swallow.playlist"
#define EMPC_SWALLOW_CONTROLS "empc.swallow.controls"
#define EMPC_SWALLOW_FILESYSTEM "empc.swallow.filesystem"
#define EMPC_SWALLOW_POPUP "empc.swallow.popup"

#define EMPC_SWALLOW_REPEAT "empc.swallow.repeat"
#define EMPC_SWALLOW_SHUFFLE "empc.swallow.shuffle"
#define EMPC_SWALLOW_SINGLE "empc.swallow.single"
#define EMPC_SWALLOW_CONSUME "empc.swallow.consume"

#define EMPC_TEXT_TRACK "empc.text.track"
#define EMPC_TEXT_TIME "empc.text.time"

#define EMPC_LOGIN_TEXT_HEADER "empc.text.login.header"
#define EMPC_LOGIN_SWALLOW_HOST "empc.swallow.login.host"
#define EMPC_LOGIN_SWALLOW_PORT "empc.swallow.login.port"
#define EMPC_LOGIN_SWALLOW_PASSWORD "empc.swallow.login.password"
